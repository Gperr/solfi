<?php include(dirname(__FILE__).'/header.php'); ?>

	<!-- ============= MAIN CONTENT ================ -->
	<main class="main grid" role="main">
		
		<!-- ======== BLOCS FULL WIDTH ======== -->
		<section class="row">
			<div class="bloc_titre_inter col-xs-12">
				<h2>Le Groupe Solfi</h2>					
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC SOLUTIONS ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Solutions globales informatiques</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="visuel col-xs-12 col-sm-3">
					<img src="data/medias/solutions_globales.jpg" alt="">
				</div>

				<div class="content col-xs-12 col-sm-9">
					<p>Fondé en mai 2000 par deux ingénieurs, le groupe a développé une offre de services intégrés pour les systèmes d’information des grands comptes.<br><br>

					Le Groupe Solfi a toujours placé la compréhension du métier et la singularité des besoins de ses clients comme problématique première.  A l’heure où toutes les entreprises ont besoin d’optimiser les coûts et recherchent des leviers de performances, le Groupe Solfi propose une solution personnalisée pour accompagner ses clients dans la transformation de leur système d’information. Cette force lui permet de s’engager pleinement dans le conseil, jusqu’à la réalisation complète de leurs projets, fondant ainsi un partenariat basé sur la confiance. <br><br>

					En étroite collaboration avec les services informatiques des grandes entreprises, le Groupe Solfi a su saisir les opportunités du marché et s’imposer grâce à la qualité de ses prestations métiers, techniques mais aussi humaines.
					</p>
				</div>
			</div>
		</section>

		<div class="row inner-page">&nbsp;</div>

		<!-- ======== BLOC CHIFFRES CLES ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Chiffres clés</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">

				<div class="content col-xs-12">
					<p>La politique « pluri-tâche » ayant permis des interventions dans la plupart des secteurs à fortes valeurs ajoutées, a favorisé la croissance du Chiffre d’affaires.<br>
					Ainsi, le Groupe Solfi a su saisir les opportunités du marché et s’imposer grâce à la qualité de ses prestations métiers, techniques mais aussi humaines.</p>
				</div>

				<div class="visuel col-xs-12">
					<img src="data/medias/graphs_chiffres_cles.jpg" alt="">
				</div>

				<div class="row">&nbsp;</div>

				<!-- <div class="content col-xs-12">
					<p>Lorem ipsum dolor sit amet, consectetur tempor incididunt ut labore et dolore magna aliqua. quis nostrud  nisi ut aliquip commodo consequat. Lorem ipsum dolor sit amet, consectetur tempor incididunt ut labore et dolore magna aliqua. quis nostrud  nisi ut aliquip commodo consequat.  Lorem ipsum dolor sit amet, consectetur tempor incididunt ut labore et dolore magna aliqua. quis nostrud  nisi ut aliquip commodo consequat.</p>
				</div> -->
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC ECOLOGIE ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Ecologie</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="row">
					<div class="visuel col-xs-12 col-sm-3">
						<img src="data/medias/visuel_ecologie.jpg" alt="">
					</div>

					<div class="content col-xs-12 col-sm-9">
						<p></p>
						<p>Le développement durable est né en 1987 et c’est la conférence de Rio en 1992 qui a édicté ses 27 principes fondateurs, dont la caractéristique commune se veut être « un mode de développement répondant aux besoins du présent, sans compromettre la capacité des générations futures à répondre aux leurs ».

						Il constitue une démarche transversale, un processus d’évolution à long terme. Le dénominateur commun est le respect de l’environnement. Les décisions s’inscrivent dans une perspective à long terme et impliquent la participation de tous les acteurs. Mais afin de les anticiper au maximum, elle privilégie une approche préventive plutôt que curative : elle intervient en amont, en instituant des mesures visant à limiter ou diminuer les éventuels impacts négatifs pour notre environnement.
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<h3 class="bleu">Politique environnementale</h3>

						<p>La protection de l’environnement est devenue de jour en jour une responsabilité citoyenne. Elle est d’autant plus importante lorsqu’une activité peut impliquer des conséquences plus ou moins indirectes, sur la dégradation de l’environnement. Le Groupe Solfi partage cette prise de conscience en adoptant une attitude qui vise à réduire l'empreinte écologique des technologies de l'information sur 3 niveaux : <br><br>

						<strong>En interne :</strong> vigilance sur la consommation en énergie du matériel et la température des salles informatiques, optimisation des ressources des machines et dématérialisation, sensibilisation des salariés, tri sélectif du papier et des consommables …<br><br>

						<strong>Avec ses clients :</strong> information sur la consommation des machines et préconisations sur l’installation des plus économiques en énergie,  récupération de l’ancien matériel lors du remplacement du parc informatique et recyclage des déchets…<br><br>

						<strong>Avec ses fournisseurs :</strong> sensibilisation des fournisseurs sur le traitement des déchets et le recyclage du matériel, préférence pour les produits reconditionnés et peu consommateur en énergie…<br>
						Développer une activité tout en respectant l’environnement est une condition sine qua non à la définition d’une entreprise en plein essor.</p>
					</div>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC VALEURS ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Valeurs</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">

				<p>La qualité de services du Groupe Solfi est due au partage d’une éthique commune parmi tous ses membres associés et employés.<br>
				Transparence, flexibilité, respect et prospective sont les maîtres mots faisant avancer le groupe.</p>
		
				<div class="row">&nbsp;</div>

				<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
					<div class="visuel col-xs-12">
						<img src="data/medias/audace.jpg" alt="">
					</div>

					<div class="content col-xs-12">
						<h3>Transparence</h3>

						<p>Toutes questions comportent des réponses. Le Groupe Solfi reste ouvert à toute interrogation, de sa structure capitalistique à son organisation interne et ses partenaires. Conscient que chaque détail a son importance, le Groupe Solfi s’engage à ce que tout son discours d’accompagnement envers ses clients et ses fournisseurs soit le plus transparent possible.</p>
					</div>
				</div>

				<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
					<div class="visuel col-xs-12">
						<img src="data/medias/audace.jpg" alt="">
					</div>

					<div class="content col-xs-12">
						<h3>Flexibilité</h3>

						<p>Apporter des solutions multiples à chaque demande, grâce à l’adaptabilité d’une équipe jeune, enthousiaste et réactive. Les engagements contractuels sont expérimentés en termes d’obligation de moyens ou de résultats. Les prestations du Groupe Solfi se fondent sur une optique de conseils pour chaque client afin de répondre à toutes les requêtes de la manière la plus efficace qui soit.</p>
					</div>
				</div>

				<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
					<div class="visuel col-xs-12">
						<img src="data/medias/audace.jpg" alt="">
					</div>

					<div class="content col-xs-12">
						<h3>Respect</h3>

						<p>Au même titre que le groupe Solfi porte une attention particulière à la volonté de ses clients, il s’engage à respecter l’environnement en tenant une attitude écologique au sein de l’entreprise mais également de sensibiliser les acteurs du secteur. </p>

					</div>
				</div>

				<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
					<div class="visuel col-xs-12">
						<img src="data/medias/audace.jpg" alt="">
					</div>

					<div class="content col-xs-12">
						<h3>Prospective</h3>

						<p>Parce que le monde évolue très vite, que les problématiques d’aujourd’hui sont déjà celles d’hier et que la recherche de la performance sera toujours le leitmotive des entreprises, le Groupe Solfi aide ses clients à préparer l’avenir en anticipant les besoins de demain.</p>

					</div>
				</div>
		
			</div>
		</section>

		<div class="row">&nbsp;</div>


	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
