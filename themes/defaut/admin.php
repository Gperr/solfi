<?php
require_once( dirname(__FILE__).'/form.lib.php' );

define( 'PHPFMG_USER', "perreau.gh@gmail.com" ); // must be a email address. for sending password to you.
define( 'PHPFMG_PW', "57fc8e" );

?>
<?php
/**
 * GNU Library or Lesser General Public License version 2.0 (LGPLv2)
*/

# main
# ------------------------------------------------------
error_reporting( E_ERROR ) ;
phpfmg_admin_main();
# ------------------------------------------------------




function phpfmg_admin_main(){
    $mod  = isset($_REQUEST['mod'])  ? $_REQUEST['mod']  : '';
    $func = isset($_REQUEST['func']) ? $_REQUEST['func'] : '';
    $function = "phpfmg_{$mod}_{$func}";
    if( !function_exists($function) ){
        phpfmg_admin_default();
        exit;
    };

    // no login required modules
    $public_modules   = false !== strpos('|captcha|', "|{$mod}|", "|ajax|");
    $public_functions = false !== strpos('|phpfmg_ajax_submit||phpfmg_mail_request_password||phpfmg_filman_download||phpfmg_image_processing||phpfmg_dd_lookup|', "|{$function}|") ;   
    if( $public_modules || $public_functions ) { 
        $function();
        exit;
    };
    
    return phpfmg_user_isLogin() ? $function() : phpfmg_admin_default();
}

function phpfmg_ajax_submit(){
    $phpfmg_send = phpfmg_sendmail( $GLOBALS['form_mail'] );
    $isHideForm  = isset($phpfmg_send['isHideForm']) ? $phpfmg_send['isHideForm'] : false;

    $response = array(
        'ok' => $isHideForm,
        'error_fields' => isset($phpfmg_send['error']) ? $phpfmg_send['error']['fields'] : '',
        'OneEntry' => isset($GLOBALS['OneEntry']) ? $GLOBALS['OneEntry'] : '',
    );
    
    @header("Content-Type:text/html; charset=$charset");
    echo "<html><body><script>
    var response = " . json_encode( $response ) . ";
    try{
        parent.fmgHandler.onResponse( response );
    }catch(E){};
    \n\n";
    echo "\n\n</script></body></html>";

}


function phpfmg_admin_default(){
    if( phpfmg_user_login() ){
        phpfmg_admin_panel();
    };
}



function phpfmg_admin_panel()
{    
    phpfmg_admin_header();
    phpfmg_writable_check();
?>    
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign=top style="padding-left:280px;">

<style type="text/css">
    .fmg_title{
        font-size: 16px;
        font-weight: bold;
        padding: 10px;
    }
    
    .fmg_sep{
        width:32px;
    }
    
    .fmg_text{
        line-height: 150%;
        vertical-align: top;
        padding-left:28px;
    }

</style>

<script type="text/javascript">
    function deleteAll(n){
        if( confirm("Are you sure you want to delete?" ) ){
            location.href = "admin.php?mod=log&func=delete&file=" + n ;
        };
        return false ;
    }
</script>


<div class="fmg_title">
    1. Email Traffics
</div>
<div class="fmg_text">
    <a href="admin.php?mod=log&func=view&file=1">view</a> &nbsp;&nbsp;
    <a href="admin.php?mod=log&func=download&file=1">download</a> &nbsp;&nbsp;
    <?php 
        if( file_exists(PHPFMG_EMAILS_LOGFILE) ){
            echo '<a href="#" onclick="return deleteAll(1);">delete all</a>';
        };
    ?>
</div>


<div class="fmg_title">
    2. Form Data
</div>
<div class="fmg_text">
    <a href="admin.php?mod=log&func=view&file=2">view</a> &nbsp;&nbsp;
    <a href="admin.php?mod=log&func=download&file=2">download</a> &nbsp;&nbsp;
    <?php 
        if( file_exists(PHPFMG_SAVE_FILE) ){
            echo '<a href="#" onclick="return deleteAll(2);">delete all</a>';
        };
    ?>
</div>

<div class="fmg_title">
    3. Form Generator
</div>
<div class="fmg_text">
    <a href="http://www.formmail-maker.com/generator.php" onclick="document.frmFormMail.submit(); return false;" title="<?php echo htmlspecialchars(PHPFMG_SUBJECT);?>">Edit Form</a> &nbsp;&nbsp;
    <a href="http://www.formmail-maker.com/generator.php" >New Form</a>
</div>
    <form name="frmFormMail" action='http://www.formmail-maker.com/generator.php' method='post' enctype='multipart/form-data'>
    <input type="hidden" name="uuid" value="<?php echo PHPFMG_ID; ?>">
    <input type="hidden" name="external_ini" value="<?php echo function_exists('phpfmg_formini') ?  phpfmg_formini() : ""; ?>">
    </form>

		</td>
	</tr>
</table>

<?php
    phpfmg_admin_footer();
}



function phpfmg_admin_header( $title = '' ){
    header( "Content-Type: text/html; charset=" . PHPFMG_CHARSET );
?>
<html>
<head>
    <title><?php echo '' == $title ? '' : $title . ' | ' ; ?>PHP FormMail Admin Panel </title>
    <meta name="keywords" content="PHP FormMail Generator, PHP HTML form, send html email with attachment, PHP web form,  Free Form, Form Builder, Form Creator, phpFormMailGen, Customized Web Forms, phpFormMailGenerator,formmail.php, formmail.pl, formMail Generator, ASP Formmail, ASP form, PHP Form, Generator, phpFormGen, phpFormGenerator, anti-spam, web hosting">
    <meta name="description" content="PHP formMail Generator - A tool to ceate ready-to-use web forms in a flash. Validating form with CAPTCHA security image, send html email with attachments, send auto response email copy, log email traffics, save and download form data in Excel. ">
    <meta name="generator" content="PHP Mail Form Generator, phpfmg.sourceforge.net">

    <style type='text/css'>
    body, td, label, div, span{
        font-family : Verdana, Arial, Helvetica, sans-serif;
        font-size : 12px;
    }
    </style>
</head>
<body  marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">

<table cellspacing=0 cellpadding=0 border=0 width="100%">
    <td nowrap align=center style="background-color:#024e7b;padding:10px;font-size:18px;color:#ffffff;font-weight:bold;width:250px;" >
        Form Admin Panel
    </td>
    <td style="padding-left:30px;background-color:#86BC1B;width:100%;font-weight:bold;" >
        &nbsp;
<?php
    if( phpfmg_user_isLogin() ){
        echo '<a href="admin.php" style="color:#ffffff;">Main Menu</a> &nbsp;&nbsp;' ;
        echo '<a href="admin.php?mod=user&func=logout" style="color:#ffffff;">Logout</a>' ;
    }; 
?>
    </td>
</table>

<div style="padding-top:28px;">

<?php
    
}


function phpfmg_admin_footer(){
?>

</div>

<div style="color:#cccccc;text-decoration:none;padding:18px;font-weight:bold;">
	:: <a href="http://phpfmg.sourceforge.net" target="_blank" title="Free Mailform Maker: Create read-to-use Web Forms in a flash. Including validating form with CAPTCHA security image, send html email with attachments, send auto response email copy, log email traffics, save and download form data in Excel. " style="color:#cccccc;font-weight:bold;text-decoration:none;">PHP FormMail Generator</a> ::
</div>

</body>
</html>
<?php
}


function phpfmg_image_processing(){
    $img = new phpfmgImage();
    $img->out_processing_gif();
}


# phpfmg module : captcha
# ------------------------------------------------------
function phpfmg_captcha_get(){
    $img = new phpfmgImage();
    $img->out();
    //$_SESSION[PHPFMG_ID.'fmgCaptchCode'] = $img->text ;
    $_SESSION[ phpfmg_captcha_name() ] = $img->text ;
}



function phpfmg_captcha_generate_images(){
    for( $i = 0; $i < 50; $i ++ ){
        $file = "$i.png";
        $img = new phpfmgImage();
        $img->out($file);
        $data = base64_encode( file_get_contents($file) );
        echo "'{$img->text}' => '{$data}',\n" ;
        unlink( $file );
    };
}


function phpfmg_dd_lookup(){
    $paraOk = ( isset($_REQUEST['n']) && isset($_REQUEST['lookup']) && isset($_REQUEST['field_name']) );
    if( !$paraOk )
        return;
        
    $base64 = phpfmg_dependent_dropdown_data();
    $data = @unserialize( base64_decode($base64) );
    if( !is_array($data) ){
        return ;
    };
    
    
    foreach( $data as $field ){
        if( $field['name'] == $_REQUEST['field_name'] ){
            $nColumn = intval($_REQUEST['n']);
            $lookup  = $_REQUEST['lookup']; // $lookup is an array
            $dd      = new DependantDropdown(); 
            echo $dd->lookupFieldColumn( $field, $nColumn, $lookup );
            return;
        };
    };
    
    return;
}


function phpfmg_filman_download(){
    if( !isset($_REQUEST['filelink']) )
        return ;
        
    $info =  @unserialize(base64_decode($_REQUEST['filelink']));
    if( !isset($info['recordID']) ){
        return ;
    };
    
    $file = PHPFMG_SAVE_ATTACHMENTS_DIR . $info['recordID'] . '-' . $info['filename'];
    phpfmg_util_download( $file, $info['filename'] );
}


class phpfmgDataManager
{
    var $dataFile = '';
    var $columns = '';
    var $records = '';
    
    function phpfmgDataManager(){
        $this->dataFile = PHPFMG_SAVE_FILE; 
    }
    
    function parseFile(){
        $fp = @fopen($this->dataFile, 'rb');
        if( !$fp ) return false;
        
        $i = 0 ;
        $phpExitLine = 1; // first line is php code
        $colsLine = 2 ; // second line is column headers
        $this->columns = array();
        $this->records = array();
        $sep = chr(0x09);
        while( !feof($fp) ) { 
            $line = fgets($fp);
            $line = trim($line);
            if( empty($line) ) continue;
            $line = $this->line2display($line);
            $i ++ ;
            switch( $i ){
                case $phpExitLine:
                    continue;
                    break;
                case $colsLine :
                    $this->columns = explode($sep,$line);
                    break;
                default:
                    $this->records[] = explode( $sep, phpfmg_data2record( $line, false ) );
            };
        }; 
        fclose ($fp);
    }
    
    function displayRecords(){
        $this->parseFile();
        echo "<table border=1 style='width=95%;border-collapse: collapse;border-color:#cccccc;' >";
        echo "<tr><td>&nbsp;</td><td><b>" . join( "</b></td><td>&nbsp;<b>", $this->columns ) . "</b></td></tr>\n";
        $i = 1;
        foreach( $this->records as $r ){
            echo "<tr><td align=right>{$i}&nbsp;</td><td>" . join( "</td><td>&nbsp;", $r ) . "</td></tr>\n";
            $i++;
        };
        echo "</table>\n";
    }
    
    function line2display( $line ){
        $line = str_replace( array('"' . chr(0x09) . '"', '""'),  array(chr(0x09),'"'),  $line );
        $line = substr( $line, 1, -1 ); // chop first " and last "
        return $line;
    }
    
}
# end of class



# ------------------------------------------------------
class phpfmgImage
{
    var $im = null;
    var $width = 73 ;
    var $height = 33 ;
    var $text = '' ; 
    var $line_distance = 8;
    var $text_len = 4 ;

    function phpfmgImage( $text = '', $len = 4 ){
        $this->text_len = $len ;
        $this->text = '' == $text ? $this->uniqid( $this->text_len ) : $text ;
        $this->text = strtoupper( substr( $this->text, 0, $this->text_len ) );
    }
    
    function create(){
        $this->im = imagecreate( $this->width, $this->height );
        $bgcolor   = imagecolorallocate($this->im, 255, 255, 255);
        $textcolor = imagecolorallocate($this->im, 0, 0, 0);
        $this->drawLines();
        imagestring($this->im, 5, 20, 9, $this->text, $textcolor);
    }
    
    function drawLines(){
        $linecolor = imagecolorallocate($this->im, 210, 210, 210);
    
        //vertical lines
        for($x = 0; $x < $this->width; $x += $this->line_distance) {
          imageline($this->im, $x, 0, $x, $this->height, $linecolor);
        };
    
        //horizontal lines
        for($y = 0; $y < $this->height; $y += $this->line_distance) {
          imageline($this->im, 0, $y, $this->width, $y, $linecolor);
        };
    }
    
    function out( $filename = '' ){
        if( function_exists('imageline') ){
            $this->create();
            if( '' == $filename ) header("Content-type: image/png");
            ( '' == $filename ) ? imagepng( $this->im ) : imagepng( $this->im, $filename );
            imagedestroy( $this->im ); 
        }else{
            $this->out_predefined_image(); 
        };
    }

    function uniqid( $len = 0 ){
        $md5 = md5( uniqid(rand()) );
        return $len > 0 ? substr($md5,0,$len) : $md5 ;
    }
    
    function out_predefined_image(){
        header("Content-type: image/png");
        $data = $this->getImage(); 
        echo base64_decode($data);
    }
    
    // Use predefined captcha random images if web server doens't have GD graphics library installed  
    function getImage(){
        $images = array(
			'F29A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGVqRxQIaWFsZHR2mOqCIiTS6NgQEBKCIMQDFAh1EkNwXGrVq6crMyKxpSO4DqpvCEAJXBxMLYGgIDA1BEWN0YGxAV8fawOjoiCYmGuoQyogiNlDhR0WIxX0A4WHMkXyw53UAAAAASUVORK5CYII=',
			'20E3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7WAMYAlhDHUIdkMREpjCGsDYwOgQgiQW0srayguSQdbeKNLqC5JDdN23aytTQVUuzkN0XgKIODBkdIGLI5rE2YNoh0oDpltBQTDcPVPhREWJxHwAG+8tEbtSXXAAAAABJRU5ErkJggg==',
			'001D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7GB0YAhimMIY6IImxBjCGMIQwOgQgiYlMYW1lBIqJIIkFtIo0OkyBi4GdFLV02sosEEJyH5o6nGIgOxjQxMBumYLqFpCbGUMdUdw8UOFHRYjFfQD3YMm4SdXaEAAAAABJRU5ErkJggg==',
			'0AE8' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7GB0YAlhDHaY6IImxBjCGsDYwBAQgiYlMYW1lBaoWQRILaBVpdEWoAzspaum0lamhq6ZmIbkPTR1UTDTUFc08kSkgdahirAGYeoEqGl3R3DxQ4UdFiMV9AHhoy+kgBXzuAAAAAElFTkSuQmCC',
			'F337' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXUlEQVR4nGNYhQEaGAYTpIn7QkNZQxhDGUNDkMQCGkRaWRsdGkRQxBiAIgHoYq0QUYT7QqNWha2aumplFpL7oOpaGTDNm4JFLIABwy2ODqhiYDejiA1U+FERYnEfALR/zggsBYB2AAAAAElFTkSuQmCC',
			'6290' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAc0lEQVR4nGNYhQEaGAYTpIn7WAMYQxhCGVqRxUSmsLYyOjpMdUASC2gRaXRtCAgIQBZrYACKBTqIILkvMmrV0pWZkVnTkNwXMoVhCkMIXB1EbytDAEMDuhijAyOaHUC3NKC7hTVANNQBzc0DFX5UhFjcBwBKp8xB84cingAAAABJRU5ErkJggg==',
			'E3C7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXklEQVR4nGNYhQEaGAYTpIn7QkNYQxhCHUNDkMQCGkRaGR2AJIoYQ6NrgwC6WCsrhIa7LzRqVdjSVatWZiG5D6qulQHDPIYpmGICAQwYbgl0wOJmFLGBCj8qQizuAwA7oszLQXnVBgAAAABJRU5ErkJggg==',
			'D290' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7QgMYQxhCGVqRxQKmsLYyOjpMdUAWaxVpdG0ICAhAEWMAigU6iCC5L2rpqqUrMyOzpiG5D6huCkMIXB1MLIChAV2M0YER3Y4prA3obgkNEA11QHPzQIUfFSEW9wEAAR3NmC9aoQMAAAAASUVORK5CYII=',
			'D38B' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAVklEQVR4nGNYhQEaGAYTpIn7QgNYQxhCGUMdkMQCpoi0Mjo6OgQgi7UyNLo2BDqIoIohqwM7KWrpqrBVoStDs5Dch6YOn3mYYljcgs3NAxV+VIRY3AcA1NbMvbROPOUAAAAASUVORK5CYII=',
			'05DB' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7GB1EQ1lDGUMdkMRYA0QaWBsdHQKQxESmAMUaAh1EkMQCWkVCQGIBSO6LWjp16dJVkaFZSO4LaGVodEWoQxETQbUDQ4w1gLUV3S2MDowh6G4eqPCjIsTiPgDJacvuMJE+PgAAAABJRU5ErkJggg==',
			'A8B7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7GB0YQ1hDGUNDkMRYA1hbWRsdGkSQxESmiDS6NgSgiAW0QtQFILkvaunKsKWhq1ZmIbkPqq4V2d7QULB5UxhQzAOLBTBg2OHogCoGdjOK2ECFHxUhFvcBAGDizTqdG3yuAAAAAElFTkSuQmCC',
			'0BB0' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYElEQVR4nGNYhQEaGAYTpIn7GB1EQ1hDGVqRxVgDRFpZGx2mOiCJiUwRaXRtCAgIQBILaAWpc3QQQXJf1NKpYUtDV2ZNQ3IfmjqYGNC8QBQxbHZgcws2Nw9U+FERYnEfANb7zMYBpEquAAAAAElFTkSuQmCC',
			'EA1D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYUlEQVR4nGNYhQEaGAYTpIn7QkMYAhimMIY6IIkFNDCGMIQwOgSgiLG2MgLFRFDERBodpsDFwE4KjZq2MguEkNyHpg4qJhqKKYZNHUQM2S2hISKNjqGOKG4eqPCjIsTiPgAzwsyUkaNsXgAAAABJRU5ErkJggg==',
			'F329' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7QkNZQxhCGaY6IIkFNIi0Mjo6BASgiDE0ujYEOoigirUyIMTATgqNWhW2amVWVBiS+8DqWhmmoultdJjC0IAhFsCAZgfQLQ4MaG5hDWENDUBx80CFHxUhFvcBAMtJzLefnhiLAAAAAElFTkSuQmCC',
			'4AC7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpI37pjAEMIQ6hoYgi4UwhjA6BDSIIIkxhrC2sjYIoIixThFpdAXSAUjumzZt2srUVatWZiG5LwCirhXZ3tBQ0VBXkO0obgGpEwhAF3N0CHRAF3MIdUQVG6jwox7E4j4ACI3MNgRSCJkAAAAASUVORK5CYII=',
			'EEC0' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAU0lEQVR4nGNYhQEaGAYTpIn7QkNEQxlCHVqRxQIaRBoYHQKmOqCJsTYIBARgiDE6iCC5LzRqatjSVSuzpiG5D00dATFMO9Ddgs3NAxV+VIRY3AcA2HXMtz2SD34AAAAASUVORK5CYII=',
			'BEF9' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXklEQVR4nGNYhQEaGAYTpIn7QgNEQ1lDA6Y6IIkFTBFpYG1gCAhAFmsFiTE6iGCog4uBnRQaNTVsaeiqqDAk90HNmyqCYR5DAxYxLHagugXsZqB5yG4eqPCjIsTiPgB3D8xpStp0pwAAAABJRU5ErkJggg==',
			'39E3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpIn7RAMYQ1hDHUIdkMQCprC2sjYwOgQgq2wVaXQF0iLIYlMgYgFI7lsZtXRpauiqpVnI7pvCGIikDmoeA6Z5rSwYYtjcgs3NAxV+VIRY3AcAYRrMUyJD2mIAAAAASUVORK5CYII=',
			'8CCC' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7WAMYQxlCHaYGIImJTGFtdHQICBBBEgtoFWlwbRB0YEFRJ9LA2sDogOy+pVHTVi1dtTIL2X1o6uDmYRPDtAPTLdjcPFDhR0WIxX0Azf3L3/ytIDMAAAAASUVORK5CYII=',
			'6451' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7WAMYWllDHVqRxUSmMExlbWCYiiwW0MIQChQLRRFrYHRlncoA0wt2UmTU0qVLM7OWIrsvZIoIUE0Aih0BraKhDhhiQLegiQHd0sroiOo+kJuBLgkNGAThR0WIxX0Ayf3L/RyrfJgAAAAASUVORK5CYII=',
			'F68D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXklEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGUMdkMQCGlhbGR0dHQJQxEQaWRsCHURQxRpA6kSQ3BcaNS1sVejKrGlI7gtoEG1FUgc3zxXTPCxi2NyC6eaBCj8qQizuAwDVPMwLI7zj/gAAAABJRU5ErkJggg==',
			'CF66' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZ0lEQVR4nGNYhQEaGAYTpIn7WENEQx1CGaY6IImJtIo0MDo6BAQgiQU0ijSwNjg6CCCLNYDEGB2Q3Re1amrY0qkrU7OQ3AdW5+iIah5Yb6CDCIYdqGLY3MIaAlSB5uaBCj8qQizuAwAIocwb2UhtaAAAAABJRU5ErkJggg==',
			'4E43' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpI37poiGMjQ6hDogi4WINDC0OjoEIIkxgsSmOjSIIImxTgHyAh0aApDcN23a1LCVmVlLs5DcFwBUx9oIVweGoaFAsdAAFPMYQOY1OmARQ3ULVjcPVPhRD2JxHwBLuM009zqZWgAAAABJRU5ErkJggg==',
			'450E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpI37poiGMkxhDA1AFgsRaWAIZXRAVscIFGN0dEQRY50iEsLaEAgTAztp2rSpS5euigzNQnJfwBSGRleEOjAMDcUUY5gi0uiIZgfDFNZWdLcA3RuC4eaBCj/qQSzuAwAJBMnQD97XXAAAAABJRU5ErkJggg==',
			'4F7D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZklEQVR4nGNYhQEaGAYTpI37poiGuoYGhjogi4WIAMlAhwAkMUaomAiSGOsUIK/RESYGdtK0aVPDVi1dmTUNyX0BIHVTGFH0hoYCeQGoYgxAdYwOmGKsQNEATDFUNw9U+FEPYnEfANiuyvW8E7dKAAAAAElFTkSuQmCC',
			'FA6A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7QkMZAhhCGVqRxQIaGEMYHR2mOqCIsbayNjgEBKCIiTS6NjA6iCC5LzRq2srUqSuzpiG5D6zO0RGmDiomGuraEBgagmFeIJo6kUZHDL0ijQ6hjChiAxV+VIRY3AcAkorNfY9NYUYAAAAASUVORK5CYII=',
			'C954' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAc0lEQVR4nGNYhQEaGAYTpIn7WEMYQ1hDHRoCkMREWllbWRsYGpHFAhpFGl0bGFpRxBqAYlMZpgQguS9q1dKlqZlZUVFI7gtoYAx0aAh0QNXL0AgUCw1BsYMFaEcAhlsYHVHdB3IzQygDithAhR8VIRb3AQAd+M6KroTImwAAAABJRU5ErkJggg==',
			'5073' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdUlEQVR4nM2QsQ2AMAwEncIbZCCP8EgJQ2QKU2QDlA0oYEoClS0oQeCXXJys18m0XUbpT3nFLycCZ2QxDBoS6SBwjOuxo2ED4iSTKIzf2Npalm0p1q/2u5nU9p0M5PpQuQbxLM4hsQbnwujOSs75q/89mBu/HVrbzMPN6mgqAAAAAElFTkSuQmCC',
			'1B23' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7GB1EQxhCGUIdkMRYHURaGR0dHQKQxEQdRBpdGwIaRFD0irQyAMUCkNy3MmtqGJBYmoXkPrA6sEoUvY0OUxjQzWt0CMAQa2V0YER1S4hoCGtoAIqbByr8qAixuA8AL13Jyy8l8XUAAAAASUVORK5CYII=',
			'4D8F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpI37poiGMIQyhoYgi4WItDI6Ojogq2MMEWl0bQhEEWOdItLoiFAHdtK0adNWZoWuDM1Ccl8AqjowDA3FNI9hClYxDLdA3YwqNlDhRz2IxX0AfzbKDQP6keYAAAAASUVORK5CYII=',
			'0F9D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7GB1EQx1CGUMdkMRYA0QaGB0dHQKQxESmiDSwNgQ6iCCJBbSiiIGdFLV0atjKzMisaUjuA6ljCMHUy4BmHsgORjQxbG5hBKlAc/NAhR8VIRb3AQALt8p5JVBCVwAAAABJRU5ErkJggg==',
			'367E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7RAMYQ1hDA0MDkMQCprC2MjQEOqCobBVpxBCbItLA0OgIEwM7aWXUtLBVS1eGZiG7b4poK8MURgzzHAIwxRwdUMVAbmFtQBUDu7mBEcXNAxV+VIRY3AcATjXJssJmffMAAAAASUVORK5CYII=',
			'9B6F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYklEQVR4nGNYhQEaGAYTpIn7WANEQxhCGUNDkMREpoi0Mjo6OiCrC2gVaXRtwBBrZW1ghImBnTRt6tSwpVNXhmYhuY/VFagOzTwGsHmBKGICWMSwuQXqZlTzBij8qAixuA8AJMnJmrL2f4QAAAAASUVORK5CYII=',
			'2C6D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7WAMYQxlCGUMdkMREprA2Ojo6OgQgiQW0ijS4Njg6iCDrBoqxNjDCxCBumjZt1dKpK7OmIbsvAKjOEVUvSBdrQyCKGGsDyA5UMaAqDLeEhmK6eaDCj4oQi/sAx5bLFJvnsUkAAAAASUVORK5CYII=',
			'2B26' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAd0lEQVR4nGNYhQEaGAYTpIn7WANEQxhCGaY6IImJTBFpZXR0CAhAEgtoFWl0bQh0EEDW3SrSygAUQ3HftKlhq1ZmpmYhuy8AqK6VEcU8RgeRRocpQBLZLQ1AsQBUMZEGoFscGFD0hoaKhrCGBqC4eaDCj4oQi/sAsYfK+GJEDIYAAAAASUVORK5CYII=',
			'E416' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7QkMYWhmmMEx1QBILaGCYyhDCEBCAKhbKGMLoIIAixujKMIXRAdl9oVFLl66atjI1C8l9AQ0iQDsY0cwTDXUA6hVBtQOkDosYqltAbmYMdUBx80CFHxUhFvcBAMQOzAOqUpYmAAAAAElFTkSuQmCC',
			'E13B' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXUlEQVR4nGNYhQEaGAYTpIn7QkMYAhhDGUMdkMQCGhgDWBsdHQJQxFgDGBoCHURQxBgCGBDqwE4KjVoVtWrqytAsJPehqUOIYTMPixi6W0JDWEPR3TxQ4UdFiMV9ANf2yyAk10iSAAAAAElFTkSuQmCC',
			'6F3A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7WANEQx1DGVqRxUSmiDSwNjpMdUASC2gRAZEBAchiDUCxRkcHEST3RUZNDVs1dWXWNCT3hUxBUQfR2wriBYaGYIqhqIO4BVUva4BIA2MoI4rYQIUfFSEW9wEAHS/Mwv/PUHkAAAAASUVORK5CYII=',
			'05AF' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7GB1EQxmmMIaGIImxBog0MIQyOiCrE5ki0sDo6IgiFtAqEsLaEAgTAzspaunUpUtXRYZmIbkvoJWh0RWhDiEWGohuB4Y61gDWVlY0MUYHxhB0sYEKPypCLO4DAEozyga5Uyb9AAAAAElFTkSuQmCC',
			'5E49' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7QkNEQxkaHaY6IIkFNIg0MLQ6BASgi011dBBBEgsMAPIC4WJgJ4VNmxq2MjMrKgzZfa0iDaxAO5D1gsVCwaYi7ACKAd2CYofIFLAYiltYAzDdPFDhR0WIxX0A8THMuNK5+rEAAAAASUVORK5CYII=',
			'0E0B' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpIn7GB1EQxmmMIY6IImxBog0MIQyOgQgiYlMEWlgdHR0EEESC2gVaWBtCISpAzspaunUsKWrIkOzkNyHpg5FTISAHdjcgs3NAxV+VIRY3AcAtz3KO9jLiwAAAAAASUVORK5CYII=',
			'6B93' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7WANEQxhCGUIdkMREpoi0Mjo6OgQgiQW0iDS6NgQ0iCCLNYi0sgLJACT3RUZNDVuZGbU0C8l9IUDzGELg6iB6W0UaHdDNA4o5oolhcws2Nw9U+FERYnEfANM9zY5eFoaiAAAAAElFTkSuQmCC',
			'2A03' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7WAMYAhimMIQ6IImJTGEMYQhldAhAEgtoZW1ldHRoEEHW3SrS6NoQ0BCA7L5p01amropamoXsvgAUdWDI6CAaChJDNo+1QaTREc0OEaCYA5pbQkOBYmhuHqjwoyLE4j4AuhvM7LiLoGsAAAAASUVORK5CYII=',
			'CC3A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7WEMYQxlDGVqRxURaWRtdGx2mOiCJBTSKNDg0BAQEIIs1iDQwNDo6iCC5L2rVtFWrpq7MmobkPjR1CLGGwNAQDDsCUdRB3IKqF+JmRhSxgQo/KkIs7gMACxvNgBoxi+gAAAAASUVORK5CYII=',
			'89B0' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7WAMYQ1hDGVqRxUSmsLayNjpMdUASC2gVaXRtCAgIQFEHFGt0dBBBct/SqKVLU0NXZk1Dcp/IFMZAJHVQ8xiA5gWiibFgsQPTLdjcPFDhR0WIxX0AyZrNfsHRhGMAAAAASUVORK5CYII=',
			'2E20' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdUlEQVR4nGNYhQEaGAYTpIn7WANEQxlCGVqRxUSmiDQwOjpMdUASC2gVaWBtCAgIQNYNFGNoCHQQQXbftKlhq1ZmZk1Ddl8AUEUrI0wdGIJ5U1DFWBuAvAAGFDtEgJDRgQHFLaGhoqGsoQEobh6o8KMixOI+ACm1yoCd0vr9AAAAAElFTkSuQmCC',
			'D590' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7QgNEQxlCGVqRxQKmiDQwOjpMdUAWaxVpYG0ICAhAFQthbQh0EEFyX9TSqUtXZkZmTUNyX0ArQ6NDCFwdQqwBXUyk0RHdjimsrehuCQ1gDEF380CFHxUhFvcBAErYzeqFPP2CAAAAAElFTkSuQmCC',
			'2218' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7WAMYQximMEx1QBITmcLayhDCEBCAJBbQKtLoGMLoIIKsu5Wh0WEKXB3ETdNWLQXiqVnI7gsA2jAF1TxGB5AoqnmsIFE0MRGgKLre0FDRUMdQBxQ3D1T4URFicR8Ayj3LF5eiBosAAAAASUVORK5CYII=',
			'01CE' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYUlEQVR4nGNYhQEaGAYTpIn7GB0YAhhCHUMDkMRYAxgDGB0CHZDViUxhDWBtEEQRC2hlAIoxwsTATopaCkIrQ7OQ3IemDqeYyBQGDDtYAxgw3MLowBqK7uaBCj8qQizuAwCp1Mb4+eBUYwAAAABJRU5ErkJggg==',
			'AE74' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7GB1EQ1lDAxoCkMRYA0SAZEAjspjIFLBYK7JYQCtQrNFhSgCS+6KWTg1btXRVVBSS+8DqpjA6IOsNDQWKBTCGhqCZx+jA0IBuB2sDuhjQzWhiAxV+VIRY3AcAdeTOEiP3qOoAAAAASUVORK5CYII=',
			'AD02' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7GB1EQximMEx1QBJjDRBpZQhlCAhAEhOZItLo6OjoIIIkFtAq0ujaENAgguS+qKXTVqauigJChPug6hqR7QgNBYu1MqCZB7RiCpoY2C2oYiA3M4aGDILwoyLE4j4A0VPNu5cavC4AAAAASUVORK5CYII=',
			'3AEB' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7RAMYAlhDHUMdkMQCpjCGsDYwOgQgq2xlbQWJiSCLTRFpdEWoAztpZdS0lamhK0OzkN2Hqg5qnmioK7p5rRB1IihuwdQrGgAUQ3PzQIUfFSEW9wEAqbjLQC/W9yEAAAAASUVORK5CYII=',
			'278D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7WANEQx1CGUMdkMREpjA0Ojo6OgQgiQW0MjS6NgQ6iCDrbmVoZQSqE0F23zQgDF2ZNQ3ZfQEMAUjqwJDRgdGBFc08VjBEFRMBQkY0t4SGAlWguXmgwo+KEIv7ANsiyjWfrpJ/AAAAAElFTkSuQmCC',
			'5A0C' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbElEQVR4nGNYhQEaGAYTpIn7QkMYAhimMEwNQBILaGAMYQhlCBBBEWNtZXR0dGBBEgsMEGl0bQh0QHZf2LRpK1NXRWahuK8VRR1UTDQUXSwAqM4RzQ6RKSKNDmhuYQXa64Dm5oEKPypCLO4DAKqVy+7qfkXnAAAAAElFTkSuQmCC',
			'8388' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAWElEQVR4nGNYhQEaGAYTpIn7WANYQxhCGaY6IImJTBFpZXR0CAhAEgtoZWh0bQh0EEFRx4CsDuykpVGrwlaFrpqaheQ+NHU4zcNuB6ZbsLl5oMKPihCL+wA3t8xGly2EVwAAAABJRU5ErkJggg==',
			'A792' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAd0lEQVR4nM2QvRGAMAhGocgGcR8s7PEuNNnALUiRDaIbWJgpzVnhT6l38nUPDt4B9VYKf8onfkidkMBMhjmG1PfEbJgvkAYdyRvGGbJTVm/84lqXbYo1Gr82xxA42RsiSEfntM8pKpcz84rN5cpAUMIP/vdiHvx20VnMyWQ6hEoAAAAASUVORK5CYII=',
			'FF88' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAUElEQVR4nGNYhQEaGAYTpIn7QkNFQx1CGaY6IIkFNIg0MDo6BASgibE2BDqI4FYHdlJo1NSwVaGrpmYhuY8U8wjYARdjQHPzQIUfFSEW9wEAl/zNW0TatTkAAAAASUVORK5CYII=',
			'90E1' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYElEQVR4nGNYhQEaGAYTpIn7WAMYAlhDHVqRxUSmMIawNjBMRRYLaGVtBYqFooqJNLo2MMD0gp00beq0lamhq5Yiu4/VFUUdBLZiiglA7MDmFhQxqJtDAwZB+FERYnEfAH/cytFY4Yp3AAAAAElFTkSuQmCC',
			'1250' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nGNYhQEaGAYTpIn7GB0YQ1hDHVqRxVgdWFtZGximOiCJiTqINLo2MAQEoOhlaHSdyuggguS+lVmrli7NzMyahuQ+oLopDA2BMHUwsQBMMUYH1oYANDtYGxgdHVDdEiIa6hDKgOLmgQo/KkIs7gMAIPXI81thC2UAAAAASUVORK5CYII=',
			'C3F9' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZ0lEQVR4nGNYhQEaGAYTpIn7WEOAMDRgqgOSmEirSCtrA0NAAJJYQCNDo2sDo4MIslgDA1AdXAzspKhVq8KWhq6KCkNyH0Qdw1Q0vUDzgHZh2MGAYgc2t4DdDDQP2c0DFX5UhFjcBwCcssvBxCDKUQAAAABJRU5ErkJggg==',
			'100D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXUlEQVR4nGNYhQEaGAYTpIn7GB0YAhimMIY6IImxOjCGMIQyOgQgiYk6sLYyOjo6iKDoFWl0bQiEiYGdtDJr2srUVZFZ05Dch6YOjxg2O7C4JQTTzQMVflSEWNwHAIQDx7f7a153AAAAAElFTkSuQmCC',
			'3D86' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAXElEQVR4nGNYhQEaGAYTpIn7RANEQxhCGaY6IIkFTBFpZXR0CAhAVtkq0ujaEOgggCw2RaTR0dHRAdl9K6OmrcwKXZmahew+iDqs5okQEMPmFmxuHqjwoyLE4j4AHfjMMveouMAAAAAASUVORK5CYII=',
			'33B3' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAW0lEQVR4nGNYhQEaGAYTpIn7RANYQ1hDGUIdkMQCpoi0sjY6OgQgq2xlaHRtCGgQQRabwgBU59AQgOS+lVGrwpaGrlqahew+VHW4zcMihs0t2Nw8UOFHRYjFfQCvPM1uYLSZYwAAAABJRU5ErkJggg==',
			'26A5' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdklEQVR4nM2QsQ2AQAhFoWAD3AcLe0wOE50Gi9tAbwMbp/TsMFpqIr974YcXYL+Nw5/yiR8pJljQNDBeKIOhxD3NPGPbXhhkdvK+k+hXyrDt4zRFP20yuTqHLgrPnV0ZeWXeS2T1wtnV6GeGqbJVfvC/F/PgdwDKTsuHIyq2dwAAAABJRU5ErkJggg==',
			'A180' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7GB0YAhhCGVqRxVgDGAMYHR2mOiCJiUxhDWBtCAgIQBILaGUAqnN0EEFyX9TSVVGrQldmTUNyH5o6MAwNZQCaF4giBlKH3Q5UtwS0soaiu3mgwo+KEIv7ABhLygtSHvjvAAAAAElFTkSuQmCC',
			'C01C' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7WEMYAhimMEwNQBITaWUMYQCKiyCJBTSyAkUZHViQxRpEGh2mMDoguy9q1bSVWUCE7D40dbjFgHYwTEG1A+yWKahuAbmZMdQBxc0DFX5UhFjcBwA4uMq0OQuGKAAAAABJRU5ErkJggg==',
			'D90A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7QgMYQximMLQiiwVMYW1lCGWY6oAs1irS6OjoEBCAJubaEOggguS+qKVLl6auisyahuS+gFbGQCR1UDEGkN7QEBQxFqAdjqjqwG5hRBGDuBlVbKDCj4oQi/sA4hXNWa+yHQkAAAAASUVORK5CYII=',
			'E7F1' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAWklEQVR4nGNYhQEaGAYTpIn7QkNEQ11DA1qRxQIaGBpdGximYhELRRNrZQViZPeFRq2atjR01VJk9wHVBSCpg4oxOmCKsTZgiolgiIWGgMVCAwZB+FERYnEfAOIBzJ3Mf1Z1AAAAAElFTkSuQmCC',
			'B0AF' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7QgMYAhimMIaGIIkFTGEMYQhldEBWF9DK2sro6IgqNkWk0bUhECYGdlJo1LSVqasiQ7OQ3IemDmoeUCwUXYy1lRVdHdAt6GIgN6OLDVT4URFicR8AObTLhsQQfQkAAAAASUVORK5CYII=',
			'7207' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdElEQVR4nM2QsRGAIAxFQ8EGuE8o7FOQhhGcIhRskBUoZEqxC6elnuZ37/4l7wL9MgJ/yit+zC6BOk6WVl+BQcLEQokRZ6ZQVqER45d7az3vm/FzCOqFqr3rBWgwtSyMpotIltHZ5LFhYgujzuyr/z2YG78DR5XLR5GUKHoAAAAASUVORK5CYII=',
			'4F50' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpI37poiGuoY6tKKIhYg0sDYwTHVAEmOEiAUEIImxTgGKTWV0EEFy37RpU8OWZmZmTUNyX8AUkIpAmDowDA3FFGMAmdcQgGIHSIzR0QHFLSAxhlAGVDcPVPhRD2JxHwCE78vAhI6KNAAAAABJRU5ErkJggg==',
			'DF55' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZklEQVR4nGNYhQEaGAYTpIn7QgNEQ11DHUMDkMQCpog0sDYwOiCrC2jFITaV0dUByX1RS6eGLc3MjIpCch9IHZBsEEHTi02MtSHQAUUM6BZGR4cAZPeFBgBVhDJMdRgE4UdFiMV9ABrLzQ9MG6IFAAAAAElFTkSuQmCC',
			'F993' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAX0lEQVR4nGNYhQEaGAYTpIn7QkMZQxhCGUIdkMQCGlhbGR0dHQJQxEQaXUEkFrEAJPeFRi1dmpkZtTQLyX0BDYyBDiFwdVAxhkYHDPNYGh0xxLC5BdPNAxV+VIRY3AcA8KbObgYtfuYAAAAASUVORK5CYII=',
			'6B4E' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7WANEQxgaHUMDkMREpoi0MrQ6OiCrC2gRaXSYiibWAFQXCBcDOykyamrYyszM0Cwk94UAzWNtRNPbKtLoGhqIIeaApg7sFjQxbG4eqPCjIsTiPgCOMsu65HDY4wAAAABJRU5ErkJggg==',
			'16BC' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZUlEQVR4nGNYhQEaGAYTpIn7GB0YQ1hDGaYGIImxOrC2sjY6BIggiYk6iDSyNgQ6sKDoFWlgbXR0QHbfyqxpYUtDV2Yhu4/RQbQVSR1Mb6Mr0DxsYqh2YHFLCKabByr8qAixuA8A4x3I34MgsYEAAAAASUVORK5CYII=',
			'632D' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcElEQVR4nGNYhQEaGAYTpIn7WANYQxhCGUMdkMREpoi0Mjo6OgQgiQW0MDS6NgQ6iCCLNTC0MiDEwE6KjFoVtmplZtY0JPeFTAGqa2VE1dvK0OgwBYtYAKoY2C0OjChuAbmZNTQQxc0DFX5UhFjcBwCYu8rieKPk4gAAAABJRU5ErkJggg==',
			'2E01' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAZElEQVR4nGNYhQEaGAYTpIn7WANEQxmmMLQii4lMEWlgCGWYiiwW0CrSwOjoEIqiGyjGCpRBcd+0qWFLV0UtRXFfAIo6MGR0wBRjbQDbgeqWBrBbUMRCQ8FuDg0YBOFHRYjFfQCb3sr7lm4+bgAAAABJRU5ErkJggg==',
			'90CF' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYElEQVR4nGNYhQEaGAYTpIn7WAMYAhhCHUNDkMREpjCGMDoEOiCrC2hlbWVtEEQTE2l0bWCEiYGdNG3qtJWpq1aGZiG5j9UVRR0EtmKKCWCxA5tboG5GNW+Awo+KEIv7ADCLyMia+oNFAAAAAElFTkSuQmCC',
			'50A7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdUlEQVR4nGNYhQEaGAYTpIn7QkMYAhimMIaGIIkFNDCGMIQyNIigiLG2Mjo6oIgFBog0ugJlApDcFzZt2srUVVErs5Dd1wpW14piM0gsNGAKslhAK2sra0NAALKYyBTGENaGQAdkMdYAhgB0sYEKPypCLO4DAJjSzGBVnKHFAAAAAElFTkSuQmCC',
			'6D4A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbUlEQVR4nGNYhQEaGAYTpIn7WANEQxgaHVqRxUSmiLQytDpMdUASC2gRaQSKBAQgizUAxQIdHUSQ3BcZNW1lZmZm1jQk94VMEWl0bYSrg+htBYqFBoaGoIk5oKkDuwVNDOJmVLGBCj8qQizuAwBR7s3BJw1C6gAAAABJRU5ErkJggg==',
			'2B96' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcElEQVR4nGNYhQEaGAYTpIn7WANEQxhCGaY6IImJTBFpZXR0CAhAEgtoFWl0bQh0EEDW3SrSygoUQ3HftKlhKzMjU7OQ3Rcg0soQEohiHqODSKMDUK8IslsaRBod0cREGjDdEhqK6eaBCj8qQizuAwAzYst0TgFZ+QAAAABJRU5ErkJggg==',
			'B4AB' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAbklEQVR4nGNYhQEaGAYTpIn7QgMYWhmmMIY6IIkFTGGYyhDK6BCALNYKFHF0dBBBUcfoytoQCFMHdlJo1NKlS1dFhmYhuS9gikgrkjqoeaKhrqGBqOa1MoDVodrBgKEX5GagGIqbByr8qAixuA8AJSXNODOqMRcAAAAASUVORK5CYII=',
			'F7C8' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7QkNFQx1CHaY6IIkFNDA0OjoEBASgibk2CDqIoIq1sjYwwNSBnRQatWra0lWrpmYhuQ8oH4CkDirG6MAKxKjmsQIhuh0iQFXobgGqQHPzQIUfFSEW9wEA79zNeQfKqm8AAAAASUVORK5CYII=',
			'73BC' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAY0lEQVR4nGNYhQEaGAYTpIn7QkNZQ1hDGaYGIIu2irSyNjoEiKCIMTS6NgQ6sCCLTWEAqnN0QHFf1KqwpaErs5Ddx+iAog4MWRsg5iGLiTRg2hHQgOmWgAYsbh6g8KMixOI+AFUBy6OpCXgNAAAAAElFTkSuQmCC',
			'D33F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAWElEQVR4nGNYhQEaGAYTpIn7QgNYQxhDGUNDkMQCpoi0sjY6OiCrC2hlaHRoCEQXA4rC1YGdFLV0VdiqqStDs5Dch6YOn3mYYljcAnUzithAhR8VIRb3AQBeecw5Zz5afQAAAABJRU5ErkJggg==',
			'C285' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAdklEQVR4nM2QMQ6AMAhFYeAG9T4wdK9JWXoaOvQGegQHe0o70uioifyE4ef/8AL02xj8SZ/wUcYMipqcFxo1FGGfSzXUaOvsGVQRiez4Su9H17MUxzdyGwpbmLuJxp68ikzjRphZbHST56O8KCvs/IP/vagHvgswQcuKrf1oCgAAAABJRU5ErkJggg==',
			'35D7' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7RANEQ1lDGUNDkMQCpog0sDY6NIggq2wFijUEoIpNEQkBiQUguW9l1NSlS1dFrcxCdt8UhkbXhoBWFJtbwWJTUMVEQGIBDChuYW1lbXR0QHUzYwjQzShiAxV+VIRY3AcAEabMs0jR+ZsAAAAASUVORK5CYII=',
			'6C25' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcklEQVR4nGNYhQEaGAYTpIn7WAMYQxlCGUMDkMREprA2Ojo6OiCrC2gRaXBtCEQVaxABkoGuDkjui4yatmrVysyoKCT3hUwBqmtlAKuG620F8qZgijkEMDogi4Hd4sAQgOw+kJtZQwOmOgyC8KMixOI+ALYgy+qB5KEAAAAAAElFTkSuQmCC',
			'DDA4' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYElEQVR4nGNYhQEaGAYTpIn7QgNEQximMDQEIIkFTBFpZQhlaEQRaxVpdHR0aEUXcwWqDkByX9TSaStTV0VFRSG5D6Iu0AFDb2hgaAimeRhuYUUTA7kZXWygwo+KEIv7AEv30VE3xIrDAAAAAElFTkSuQmCC',
			'8365' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7WANYQxhCGUMDkMREpoi0Mjo6OiCrC2hlaHRtQBUTmcLQytrA6OqA5L6lUavClk5dGRWF5D6wOkeHBhEM8wKwiAU6iGC4xSEA2X0QNzNMdRgE4UdFiMV9ADNDy6PPnHd5AAAAAElFTkSuQmCC',
			'51E8' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYUlEQVR4nGNYhQEaGAYTpIn7QkMYAlhDHaY6IIkFNDAGsDYwBASgiLECxRgdRJDEAgMYkNWBnRQ2bVXU0tBVU7OQ3dfKgGEeRAzVvAAsYiJTMPUCXRKK7uaBCj8qQizuAwAGIsmEPgIENQAAAABJRU5ErkJggg==',
			'05BE' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7GB1EQ1lDGUMDkMRYA0QaWBsdHZDViUwBijUEoogFtIqEIKkDOylq6dSlS0NXhmYhuS+glaHRFc08sBiaeUA7MMRYA1hb0d3C6MAYgu7mgQo/KkIs7gMA6qjKb9M6PoMAAAAASUVORK5CYII=',
			'B28F' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7QgMYQxhCGUNDkMQCprC2Mjo6OiCrC2gVaXRtCEQVm8LQ6IhQB3ZSaNSqpatCV4ZmIbkPqG4KpnkMAazo5rUyOmCITWFtQNcbGiAa6hDKiCI2UOFHRYjFfQCHHsq1NjI/kQAAAABJRU5ErkJggg==',
			'BAF2' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAa0lEQVR4nGNYhQEaGAYTpIn7QgMYAlhDA6Y6IIkFTGEMYW1gCAhAFmtlbWVtYHQQQVEn0ugKpEWQ3BcaNW1lauiqVVFI7oOqa0Sxo1U0FCjWyoAiBlY3hQHTjgBUN4PEGENDBkH4URFicR8AOZjOADCV5mwAAAAASUVORK5CYII=',
			'A0FE' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAYElEQVR4nGNYhQEaGAYTpIn7GB0YAlhDA0MDkMRYAxhDWEEySGIiU1hb0cUCWkUaXRFiYCdFLZ22MjV0ZWgWkvvQ1IFhaCimWEArNjsw3RLQCnRzAyOKmwcq/KgIsbgPAISRyX4JOiRDAAAAAElFTkSuQmCC',
			'D752' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAcUlEQVR4nGNYhQEaGAYTpIn7QgNEQ11DHaY6IIkFTGFodG1gCAhAFmsFiTE6iKCKtbJOZWgQQXJf1NJV05ZmZq2KQnIfUF0AkGxEsaOV0QEsgyLG2sAKsh3FLSINjI4OAahuBtoYyhgaMgjCj4oQi/sAnXrN7L8CHpUAAAAASUVORK5CYII=',
			'AA08' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7GB0YAhimMEx1QBJjDWAMYQhlCAhAEhOZwtrK6OjoIIIkFtAq0ujaEABTB3ZS1NJpK1NXRU3NQnIfmjowDA0VDXVtCMQwzxGLHQ5obgGLobl5oMKPihCL+wAc7s2C7UpZNQAAAABJRU5ErkJggg==',
			'F24A' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaklEQVR4nGNYhQEaGAYTpIn7QkMZQxgaHVqRxQIaWFsZWh2mOqCIiTQCRQICUMSAOgMdHUSQ3BcatWrpyszMrGlI7gOqm8LaCFcHEwtgDQ0MDUERY3RgwFDH2oApJhrqgCY2UOFHRYjFfQA2ms2jhg4jBwAAAABJRU5ErkJggg==',
			'1099' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaUlEQVR4nGNYhQEaGAYTpIn7GB0YAhhCGaY6IImxOjCGMDo6BAQgiYk6sLayNgQ6iKDoFWl0RYiBnbQya9rKzMyoqDAk94HUOYQETEXX69AQ0IAqxtrK2BCAZgcWt4Rgunmgwo+KEIv7AJjcyKHV7VEtAAAAAElFTkSuQmCC',
			'CFAB' => 'iVBORw0KGgoAAAANSUhEUgAAAEkAAAAhAgMAAADoum54AAAACVBMVEX///8AAADS0tIrj1xmAAAAaElEQVR4nGNYhQEaGAYTpIn7WENEQx2mMIY6IImJtIo0MIQyOgQgiQU0ijQwOjo6iCCLNYg0sDYEwtSBnRS1amrY0lWRoVlI7kNThxALDUQ1rxGiTgTNLeh6WUPAYihuHqjwoyLE4j4AZIDMjgNB7CwAAAAASUVORK5CYII='        
        );
        $this->text = array_rand( $images );
        return $images[ $this->text ] ;    
    }
    
    function out_processing_gif(){
        $image = dirname(__FILE__) . '/processing.gif';
        $base64_image = "R0lGODlhFAAUALMIAPh2AP+TMsZiALlcAKNOAOp4ANVqAP+PFv///wAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAIACwAAAAAFAAUAAAEUxDJSau9iBDMtebTMEjehgTBJYqkiaLWOlZvGs8WDO6UIPCHw8TnAwWDEuKPcxQml0Ynj2cwYACAS7VqwWItWyuiUJB4s2AxmWxGg9bl6YQtl0cAACH5BAUKAAgALAEAAQASABIAAAROEMkpx6A4W5upENUmEQT2feFIltMJYivbvhnZ3Z1h4FMQIDodz+cL7nDEn5CH8DGZhcLtcMBEoxkqlXKVIgAAibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkphaA4W5upMdUmDQP2feFIltMJYivbvhnZ3V1R4BNBIDodz+cL7nDEn5CH8DGZAMAtEMBEoxkqlXKVIg4HibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpjaE4W5tpKdUmCQL2feFIltMJYivbvhnZ3R0A4NMwIDodz+cL7nDEn5CH8DGZh8ONQMBEoxkqlXKVIgIBibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpS6E4W5spANUmGQb2feFIltMJYivbvhnZ3d1x4JMgIDodz+cL7nDEn5CH8DGZgcBtMMBEoxkqlXKVIggEibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpAaA4W5vpOdUmFQX2feFIltMJYivbvhnZ3V0Q4JNhIDodz+cL7nDEn5CH8DGZBMJNIMBEoxkqlXKVIgYDibbK9YLBYvLtHH5K0J0IACH5BAUKAAgALAEAAQASABIAAAROEMkpz6E4W5tpCNUmAQD2feFIltMJYivbvhnZ3R1B4FNRIDodz+cL7nDEn5CH8DGZg8HNYMBEoxkqlXKVIgQCibbK9YLBYvLtHH5K0J0IACH5BAkKAAgALAEAAQASABIAAAROEMkpQ6A4W5spIdUmHQf2feFIltMJYivbvhnZ3d0w4BMAIDodz+cL7nDEn5CH8DGZAsGtUMBEoxkqlXKVIgwGibbK9YLBYvLtHH5K0J0IADs=";
        $binary = is_file($image) ? join("",file($image)) : base64_decode($base64_image); 
        header("Cache-Control: post-check=0, pre-check=0, max-age=0, no-store, no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: image/gif");
        echo $binary;
    }

}
# end of class phpfmgImage
# ------------------------------------------------------
# end of module : captcha


# module user
# ------------------------------------------------------
function phpfmg_user_isLogin(){
    return ( isset($_SESSION['authenticated']) && true === $_SESSION['authenticated'] );
}


function phpfmg_user_logout(){
    session_destroy();
    header("Location: admin.php");
}

function phpfmg_user_login()
{
    if( phpfmg_user_isLogin() ){
        return true ;
    };
    
    $sErr = "" ;
    if( 'Y' == $_POST['formmail_submit'] ){
        if(
            defined( 'PHPFMG_USER' ) && strtolower(PHPFMG_USER) == strtolower($_POST['Username']) &&
            defined( 'PHPFMG_PW' )   && strtolower(PHPFMG_PW) == strtolower($_POST['Password']) 
        ){
             $_SESSION['authenticated'] = true ;
             return true ;
             
        }else{
            $sErr = 'Login failed. Please try again.';
        }
    };
    
    // show login form 
    phpfmg_admin_header();
?>
<form name="frmFormMail" action="" method='post' enctype='multipart/form-data'>
<input type='hidden' name='formmail_submit' value='Y'>
<br><br><br>

<center>
<div style="width:380px;height:260px;">
<fieldset style="padding:18px;" >
<table cellspacing='3' cellpadding='3' border='0' >
	<tr>
		<td class="form_field" valign='top' align='right'>Email :</td>
		<td class="form_text">
            <input type="text" name="Username"  value="<?php echo $_POST['Username']; ?>" class='text_box' >
		</td>
	</tr>

	<tr>
		<td class="form_field" valign='top' align='right'>Password :</td>
		<td class="form_text">
            <input type="password" name="Password"  value="" class='text_box'>
		</td>
	</tr>

	<tr><td colspan=3 align='center'>
        <input type='submit' value='Login'><br><br>
        <?php if( $sErr ) echo "<span style='color:red;font-weight:bold;'>{$sErr}</span><br><br>\n"; ?>
        <a href="admin.php?mod=mail&func=request_password">I forgot my password</a>   
    </td></tr>
</table>
</fieldset>
</div>
<script type="text/javascript">
    document.frmFormMail.Username.focus();
</script>
</form>
<?php
    phpfmg_admin_footer();
}


function phpfmg_mail_request_password(){
    $sErr = '';
    if( $_POST['formmail_submit'] == 'Y' ){
        if( strtoupper(trim($_POST['Username'])) == strtoupper(trim(PHPFMG_USER)) ){
            phpfmg_mail_password();
            exit;
        }else{
            $sErr = "Failed to verify your email.";
        };
    };
    
    $n1 = strpos(PHPFMG_USER,'@');
    $n2 = strrpos(PHPFMG_USER,'.');
    $email = substr(PHPFMG_USER,0,1) . str_repeat('*',$n1-1) . 
            '@' . substr(PHPFMG_USER,$n1+1,1) . str_repeat('*',$n2-$n1-2) . 
            '.' . substr(PHPFMG_USER,$n2+1,1) . str_repeat('*',strlen(PHPFMG_USER)-$n2-2) ;


    phpfmg_admin_header("Request Password of Email Form Admin Panel");
?>
<form name="frmRequestPassword" action="admin.php?mod=mail&func=request_password" method='post' enctype='multipart/form-data'>
<input type='hidden' name='formmail_submit' value='Y'>
<br><br><br>

<center>
<div style="width:580px;height:260px;text-align:left;">
<fieldset style="padding:18px;" >
<legend>Request Password</legend>
Enter Email Address <b><?php echo strtoupper($email) ;?></b>:<br />
<input type="text" name="Username"  value="<?php echo $_POST['Username']; ?>" style="width:380px;">
<input type='submit' value='Verify'><br>
The password will be sent to this email address. 
<?php if( $sErr ) echo "<br /><br /><span style='color:red;font-weight:bold;'>{$sErr}</span><br><br>\n"; ?>
</fieldset>
</div>
<script type="text/javascript">
    document.frmRequestPassword.Username.focus();
</script>
</form>
<?php
    phpfmg_admin_footer();    
}


function phpfmg_mail_password(){
    phpfmg_admin_header();
    if( defined( 'PHPFMG_USER' ) && defined( 'PHPFMG_PW' ) ){
        $body = "Here is the password for your form admin panel:\n\nUsername: " . PHPFMG_USER . "\nPassword: " . PHPFMG_PW . "\n\n" ;
        if( 'html' == PHPFMG_MAIL_TYPE )
            $body = nl2br($body);
        mailAttachments( PHPFMG_USER, "Password for Your Form Admin Panel", $body, PHPFMG_USER, 'You', "You <" . PHPFMG_USER . ">" );
        echo "<center>Your password has been sent.<br><br><a href='admin.php'>Click here to login again</a></center>";
    };   
    phpfmg_admin_footer();
}


function phpfmg_writable_check(){
 
    if( is_writable( dirname(PHPFMG_SAVE_FILE) ) && is_writable( dirname(PHPFMG_EMAILS_LOGFILE) )  ){
        return ;
    };
?>
<style type="text/css">
    .fmg_warning{
        background-color: #F4F6E5;
        border: 1px dashed #ff0000;
        padding: 16px;
        color : black;
        margin: 10px;
        line-height: 180%;
        width:80%;
    }
    
    .fmg_warning_title{
        font-weight: bold;
    }

</style>
<br><br>
<div class="fmg_warning">
    <div class="fmg_warning_title">Your form data or email traffic log is NOT saving.</div>
    The form data (<?php echo PHPFMG_SAVE_FILE ?>) and email traffic log (<?php echo PHPFMG_EMAILS_LOGFILE?>) will be created automatically when the form is submitted. 
    However, the script doesn't have writable permission to create those files. In order to save your valuable information, please set the directory to writable.
     If you don't know how to do it, please ask for help from your web Administrator or Technical Support of your hosting company.   
</div>
<br><br>
<?php
}


function phpfmg_log_view(){
    $n = isset($_REQUEST['file'])  ? $_REQUEST['file']  : '';
    $files = array(
        1 => PHPFMG_EMAILS_LOGFILE,
        2 => PHPFMG_SAVE_FILE,
    );
    
    phpfmg_admin_header();
   
    $file = $files[$n];
    if( is_file($file) ){
        if( 1== $n ){
            echo "<pre>\n";
            echo join("",file($file) );
            echo "</pre>\n";
        }else{
            $man = new phpfmgDataManager();
            $man->displayRecords();
        };
     

    }else{
        echo "<b>No form data found.</b>";
    };
    phpfmg_admin_footer();
}


function phpfmg_log_download(){
    $n = isset($_REQUEST['file'])  ? $_REQUEST['file']  : '';
    $files = array(
        1 => PHPFMG_EMAILS_LOGFILE,
        2 => PHPFMG_SAVE_FILE,
    );

    $file = $files[$n];
    if( is_file($file) ){
        phpfmg_util_download( $file, PHPFMG_SAVE_FILE == $file ? 'form-data.csv' : 'email-traffics.txt', true, 1 ); // skip the first line
    }else{
        phpfmg_admin_header();
        echo "<b>No email traffic log found.</b>";
        phpfmg_admin_footer();
    };

}


function phpfmg_log_delete(){
    $n = isset($_REQUEST['file'])  ? $_REQUEST['file']  : '';
    $files = array(
        1 => PHPFMG_EMAILS_LOGFILE,
        2 => PHPFMG_SAVE_FILE,
    );
    phpfmg_admin_header();

    $file = $files[$n];
    if( is_file($file) ){
        echo unlink($file) ? "It has been deleted!" : "Failed to delete!" ;
    };
    phpfmg_admin_footer();
}


function phpfmg_util_download($file, $filename='', $toCSV = false, $skipN = 0 ){
    if (!is_file($file)) return false ;

    set_time_limit(0);


    $buffer = "";
    $i = 0 ;
    $fp = @fopen($file, 'rb');
    while( !feof($fp)) { 
        $i ++ ;
        $line = fgets($fp);
        if($i > $skipN){ // skip lines
            if( $toCSV ){ 
              $line = str_replace( chr(0x09), ',', $line );
              $buffer .= phpfmg_data2record( $line, false );
            }else{
                $buffer .= $line;
            };
        }; 
    }; 
    fclose ($fp);
  

    
    /*
        If the Content-Length is NOT THE SAME SIZE as the real conent output, Windows+IIS might be hung!!
    */
    $len = strlen($buffer);
    $filename = basename( '' == $filename ? $file : $filename );
    $file_extension = strtolower(substr(strrchr($filename,"."),1));

    switch( $file_extension ) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "jpeg":
        case "jpg": $ctype="image/jpg"; break;
        case "mp3": $ctype="audio/mpeg"; break;
        case "wav": $ctype="audio/x-wav"; break;
        case "mpeg":
        case "mpg":
        case "mpe": $ctype="video/mpeg"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
        case "php":
        case "htm":
        case "html": 
                $ctype="text/plain"; break;
        default: 
            $ctype="application/x-download";
    }
                                            

    //Begin writing headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public"); 
    header("Content-Description: File Transfer");
    //Use the switch-generated Content-Type
    header("Content-Type: $ctype");
    //Force the download
    header("Content-Disposition: attachment; filename=".$filename.";" );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$len);
    
    while (@ob_end_clean()); // no output buffering !
    flush();
    echo $buffer ;
    
    return true;
 
    
}
?>