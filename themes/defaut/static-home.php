<?php include(dirname(__FILE__).'/header.php'); ?>

	<!-- ============= MAIN CONTENT TABLETTES ================ -->
	<main class="main grid" role="main">
		
		<!-- ======== BLOCS FULL WIDTH ======== -->
		<section class="row hero">
			<div class="full-width-bloc col-xs-12">
				<div class="visuel_wrapper col-sm-7 col-sm-push-5 col-xl-7">
					<img class="visuel_home" src="data/medias/visuel_home.jpg" alt="">
				</div>

				<div class="content-left col-sm-5 col-sm-pull-7 col-xl-5">
					<h2>Votre partenaire informatique jour après jour</h2>
					<p>Aujourd’hui, quelle que soit la structure de l’entreprise, l’amélioration de ses performances passe par l’informatique. La digitalisation croissante avec la consumérisation notamment l’arrivée des smartphones et tablettes, les réseaux sociaux, les services de cloud, transforme l’architecture des systèmes d’information. Cela engendre de nouvelles problématiques en termes de disponibilité, compatibilité et sécurité des données.<br><br>

					A travers ce bouleversement technologique, le Groupe Solfi propose un accompagnement sur-mesure qui permet de déployer une offre adaptée aux capacités de l’entreprise, aussi bien termes de structure, compétences techniques que de budget. Il conseille ses clients pour définir la meilleure stratégie d’un point de vue équipement et développement informatique à travers 2 offres : SGI solfi et SGI ingénierie. 
					</p>

					<a href="<?php $plxShow->staticUrl() ?>?static2/offres-groupe-solfi" class="btn_bleu">Voir nos offres</a>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOCS 50 / 50 ======== -->
		<section class="row blackbg">
			<!-- ======== BLOCS LEFT ======== -->
			<div class="bloc_2articles col-xs-12 col-sm-6">
				
				<div class="visuel col-xs-12 col-sm-12 col-lg-6">
					<img src="data/medias/visuel-.jpg" alt="">
				</div>

				<div class="content col-xs-12 col-sm-12 col-lg-6">
						<h3>«une relation de confiance s’est installée et je sais que je peux compter sur eux…»</h3>

						<p>Nous sommes client du Groupe Solfi depuis 15 ans maintenant et je dois dire que je n’ai jamais pensé à changer de prestataire...</p>
					</div>
				</div>
			</div>
			
			<!-- ======== BLOCS RIGHT ======== -->
			<div class="bloc_2articles col-xs-12 col-sm-6 float-right">
				<div class="visuel col-xs-12 col-sm-12 col-lg-6">
					<img src="data/medias/visuel-.jpg" alt="">
				</div>

				<div class="content col-xs-12 col-sm-12 col-lg-6">
					<h3>«Très vite nous avons senti que nous étions face à une équipe compétente»</h3>

					<p>Nous avons rencontré le groupe Solfi pour le renouvellement de notre parc informatique. Très vite nous avons senti que nous étions face à une équipe compétente...</p>

				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOCS 25 / 25 / 25 /25 ======== -->
		<section class="row blackbg">
			<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
				<div class="visuel col-xs-12">
					<img src="data/medias/audace.jpg" alt="">
				</div>

				<div class="content col-xs-12">
					<h3>Transparence</h3>

					<p>Toutes questions comportent des réponses. Le Groupe Solfi reste ouvert à toute interrogation, de sa structure capitalistique à son organisation interne et ses partenaires. Conscient que chaque détail a son importance, le Groupe Solfi s’engage à ce que tout ...</p>

				</div>
			</div>

			<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
				<div class="visuel col-xs-12">
					<img src="data/medias/audace.jpg" alt="">
				</div>

				<div class="content col-xs-12">
					<h3>Flexibilité</h3>

					<p>Apporter des solutions multiples à chaque demande, grâce à l’adaptabilité d’une équipe jeune, enthousiaste et réactive. Les engagements contractuels sont expérimentés en termes d’obligation de moyens ou de résultats. Les prestations du Groupe Solfi...</p>

				</div>
			</div>

			<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
				<div class="visuel col-xs-12">
					<img src="data/medias/audace.jpg" alt="">
				</div>

				<div class="content col-xs-12">
					<h3>Respect</h3>

					<p>Au même titre que le groupe Solfi porte une attention particulière à la volonté de ses clients, il s’engage à respecter l’environnement en tenant une attitude écologique au sein de l’entreprise mais également de sensibiliser les acteurs du secteur. </p>

				</div>
			</div>

			<div class="bloc_4articles col-xs-12 col-sm-6 col-lg-3">
				<div class="visuel col-xs-12">
					<img src="data/medias/audace.jpg" alt="">
				</div>

				<div class="content col-xs-12">
					<h3>Prospective</h3>

					<p>Parce que le monde évolue très vite, que les problématiques d’aujourd’hui sont déjà celles d’hier et que la recherche de la performance sera toujours le leitmotive des entreprises, le Groupe Solfi aide ses clients à préparer l’avenir en anticipant les besoins de demain.</p>

				</div>
			</div>
		</section>


	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
