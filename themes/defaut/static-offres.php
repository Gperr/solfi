<?php include(dirname(__FILE__).'/header.php'); ?>
<link rel="stylesheet" href="themes/defaut/js/stacktable.js/stacktable.css">

	<!-- ============= MAIN CONTENT ================ -->
	<main class="main grid" role="main">
		
		<!-- ======== BLOCS FULL WIDTH ======== -->
		<section class="row">
			<div class="bloc_titre_inter col-xs-12">
				<h2>Les offres du Groupe Solfi</h2>					
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC SOLUTIONS ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Solutions globales informatiques</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="content col-xs-12">
					<p>Aujourd’hui, quelle que soit la structure de l’entreprise, l’amélioration de ses performances passe par l’informatique. La digitalisation croissante avec la consumérisation notamment l’arrivée des smartphones et tablettes, les réseaux sociaux, les services de cloud, transforme l’architecture des systèmes d’information. Cela engendre de nouvelles problématiques en termes de disponibilité, compatibilité et sécurité des données.<br><br>

					L’infrastructure doit être adaptée pour satisfaire les demandes toujours plus nombreuses, des utilisateurs. Il s’agit essentiellement de leur mettre à disposition de façon rapide, les services et applications métiers, tout en étant performant et sécurisé. <br><br>

					A travers ce bouleversement technologique, le Groupe Solfi propose un accompagnement sur-mesure qui permet de déployer une offre adaptée aux capacités de l’entreprise, aussi bien termes de structure et compétences techniques que de budget. Il conseille ses clients pour définir la meilleure stratégie d’un point de vue équipement et développement informatique à travers 2 offres : <a href="#SGI"><strong>SGI solfi</strong></a> et <a href="#ingenierie"><strong>SGI ingénierie</strong></a>. 
					</p>
				</div>
				<div class="col-xs-12"><img src="data/medias/demarche-groupe-solfi.jpg" alt="Démarche Groupe Solfi"></div>
			</div>
		</section>

		<div class="row inner-page">&nbsp;</div>

		<!-- ======== BLOC CHIFFRES CLES ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1 id="SGI">Solfi SGI</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="content col-xs-12 col-sm-3">
					<img src="data/medias/solfi-SGI.jpg" alt="Solfi SGI">

					<div class="row">&nbsp;</div>

					<img src="data/medias/visuel-sgi.jpg" alt="">
				</div>


				<div class="content col-xs-12 col-sm-9">
					<p>Spécialisé initialement dans l’upgrade informatique, Solfi SGI assure dorénavant une polyvalence dans chaque prestation, se faisant garant d’une adaptabilité « au cas par cas » pour chaque infrastructure ou site client déployé dans le but de devenir jour après jour, LE partenaire informatique de VOS futurs projets.<br><br>

					- Unités centrales, écran tactiles, portables, tablettes, serveurs, imprimantes et imprimantes 3D<br>
					- Extensions informatiques et périphériques<br>
					- Stockage et réseaux<br>
					- Matériel de sauvegarde<br>
					- Téléphonie d’entreprise (fixe et mobile)<br>
					- Sécurité<br>
					- Logiciels<br>
					- Installation et maintenance<br>
					- Financement, location évolutive…</p>

					<table class="tableau_bleu">
						<thead>
							<tr>
								<td>Pôle</td>
								<td>Compétences</td>
							</tr>
						</thead>
						<tr>
							<td class="bleu"><strong>Consulting services achats</strong></td>
							<td>
								<ul class="blue_bullets">
									<li>Une distribution dans toute l'Europe</li>
									<li>Le financement, la location évolutive</li>
									<li>L'intégration, la mesterisation, le déploiement</li>
									<li>La souplesse, la réactivité, la disponibilité</li>
									<li>Les accords avec tous les contructeurs et éditeurs du marché (HP, Dell, IBM)</li>
								</ul>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC VALEURS ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1 id="ingenierie">SGI Ingénierie</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">

				<div class="visuel col-xs-12 col-sm-3">
					<img style="margin-top: 50px; margin-bottom: 40px; width: 235px;" src="data/medias/SGI-ingenierie.svg" alt="SGI Ingénierie">

					<img src="data/medias/visuel_sgi_ingenierie.jpg" alt="">
				</div>

				<div class="content tab_wrapper col-xs-12 col-sm-9">
					<p></p>
					<p>Le Groupe Solfi a créé SGI Ingénierie afin d’accompagner ses clients dans l’amélioration de leurs performances stratégiques et opérationnelles à travers la conduite et la réalisation de leurs projets informatiques.<br><br>

					Il s’appuie sur le savoir-faire de consultants spécialisés dans l’informatique de gestion. Ces derniers s’adaptent aux enjeux, à la problématique mais aussi aux particularités de chacun de ses clients afin de mieux les accompagner dans les diverses phases de leurs projets, couvrant de la maîtrise d’oeuvre jusqu’à la validation. <br><br>

					L’assistance technique, le conseil, ainsi que la mise en place de productions font du Groupe Solfi une structure tendant vers une parfaite maîtrise des techniques informatiques liées aux exigences des entreprises. La valeur ajoutée de ses consultants ainsi que de ses méthodes de développement de plateformes, assurent la pérennité des missions et instaurent une relation de confiance.
					</p>

					<table class="tableau_bleu second">
						<thead>
							<tr>
								<td >Pôle</td>
								<td  colspan="2">Compétences</td>
								<td >Métiers</td>
							</tr>
						</thead>
						<tr>
							<td class="bleugras">Maîtrise d'ouvrage MOA</td>
							<td colspan="2">
								<ul class="blue_bullets">
									<li>Moyens de paiement, monétique, finance, finance de marché</li>
									<li>Assurance vie, IARD, logistique comptabilité, titres, fiscalité, risques. </li>
									<li>Crédits, règlementaire, ressources humaines, paie, marketing.</li>
									<li>Communication, développement durable.</li>
								</ul>
							</td>
							<td valign="top">Consulting<br>
							Audit<br>
							Expertise<br>
							Direction de projet<br>
							Coordination<br>
							Conduite au changement<br>
							Homologation</td>
						</tr>
						<tr>
							<td class="bleugras" rowspan="5">Maîtrise d'oeuvre MOE</td>
							<td class="bleugras">Nouvelles technologies</td>
							<td>DotNet, Asp.Net, VB.Net, Java, J2EE, Struts, Websphere, Spring, Jrulen, Scrum, Weblogic, Hibernate, Tomcat, Eclipse, Php, C3 </td>
							<td rowspan="5">Expert<br>
							Directeur de projet<br>
							Chef de projet<br>
							Architecte<br>
							DBA<br>
							Analyste<br>
							Concepteur<br>
							Développeur</td>
						</tr>
						<tr>
							<td class="bleugras">Client - Serveur</td>
							<td>C, C++, Java, Powerbuilder, Vb, Delphi, Sql, T-Sql, PL/SQL, Sybase, Oracle, SQL-Server, Shell  </td>
						</tr>
						<tr>
							<td class="bleugras">Mainframe</td>
							<td>Cobol, CICS, IMS, Pacbase, DB2</td>
						</tr>
						<tr>
							<td class="bleugras">Décisionnel</td>
							<td>BO, Microstrategy, Datastage, Px, Informatica, Genio, Datastage TX, SAS, Hyperion </td>
						</tr>
						<tr>
							<td class="bleugras">ERP</td>
							<td>SAP, Peoplesoft, HS Access, Siebel, Oracle application </td>
						</tr>

						<tr>
							<td class="bleugras" rowspan="2">Bureautique</td>
							<td class="bleugras">Poste de travail</td>
							<td>Windows XP, Windows Seven, Outlook, Lotus Notes, Pack office, 3G, VPN</td>
							<td rowspan="2">Technicien Helpdesk<br>
							Technicien Support<br>
							Packager / Intégrateur</td>
						</tr>
						<tr>
							<td class="bleugras">Packaging Intégration</td>
							<td>OnstallShield, Admin Studio, Wise Package Studio, Télédistribution (Altiris, sms/sccm, Landesk) </td>
						</tr>

						<tr>
							<td class="bleugras" rowspan="5">Exploitation <br>Production</td>
							<td class="bleugras">Système</td>
							<td>Windows NT/2000/2003/2008, AD, Unix (Solaris, AIX, HP-UX), LDAP, Linus (Redhat, Fedora ...)</td>
							<td rowspan="5">Technicien<br>
							Analyste<br>
							Administrateur<br>
							Ingénieur<br>
							Chef de projet</td>
						</tr>
						<tr>
							<td class="bleugras">Réseaux</td>
							<td>LAN, MAN, WAN (MPLS...) </td>
						</tr>
						<tr>
							<td class="bleugras">Messagerie</td>
							<td>Exchange 2003/2007, Lotus Domino...</td>
						</tr>
						<tr>
							<td class="bleugras">Sauvegarde archivage</td>
							<td>NetBackup, BackupExec, Entreprise Vault, Time navigator</td>
						</tr>
						<tr>
							<td class="bleugras">Supervision</td>
							<td>HP-OV, Nagios, Ciscoworks</td>
						</tr>

						<tr>
							<td class="bleugras" rowspan="2">Sécurité</td>
							<td class="bleugras">Réseaux</td>
							<td>Firewall (Checkpoint, Cisco ASA, Fortigate), Antispam (Squid), Sondes (IDS/IPS)... </td>
							<td rowspan="2">Technicien<br>
							Administrateur<br>
							Ingénieur</td>
						</tr>
						<tr>
							<td class="bleugras">Système</td>
							<td>Habilitation (Racf, Kerberos...)</td>
						</tr>

						<tr>
							<td class="bleugras">Base de données</td>
							<td colspan="2">Oracle, Sybase, SQL Server, DB2</td>
							<td>DBA<br>
							Architecte</td>
						</tr>
					</table>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>


	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
<script type="text/javascript" src="themes/defaut/js/stacktable.js/stacktable.js"></script>
<script>
	$('.tableau_bleu.second').cardtable();
</script>