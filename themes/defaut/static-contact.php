<?php include(dirname(__FILE__).'/header.php'); ?>
<?php

// if the from is loaded from WordPress form loader plugin,
// the phpfmg_display_form() will be called by the loader
if( !defined('FormmailMakerFormLoader') ){
    # This block must be placed at the very top of page.
    # --------------------------------------------------
	require_once( dirname(__FILE__).'/form.lib.php' );
    phpfmg_display_form();
    # --------------------------------------------------
};


function phpfmg_form( $sErr = false ){
		$style=" class='form_text' ";

?>

<main class="main grid" role="main">
    <section class="row">
        <div class="bloc_titre_inter col-xs-12">
            <h2>Une candidature ? Un renseignement ?</h2>                   
        </div>
    </section>

    <div class="row">&nbsp;</div>

    <section class="row inner-page">
        <div class="col-xs-6 col-sm-3">
            <center><img src="data/medias/visuel_contact.jpg" alt=""></center>
        </div>
        <div class="col-xs-12 col-sm-9">
            <p>Le Groupe solfi se tient à votre disposition. Pour tout renseignement ou candidature spontanée, veuillez remplir le formulaire ci-dessous. </p>
            <!-- Début du formulaire -->
            <div id='frmFormMailContainer'>
                <form name="frmFormMail" id="frmFormMail" target="submitToFrame" action='<?php echo PHPFMG_ADMIN_URL . '' ; ?>' method='post' enctype='multipart/form-data' onsubmit='return fmgHandler.onSubmit(this);'>

                    <input type='hidden' name='formmail_submit' value='Y'>
                    <input type='hidden' name='mod' value='ajax'>
                    <input type='hidden' name='func' value='submit'>

                                
                    <ol class='phpfmg_form' >

                        <li class='field_block' id='field_0_div'><div class='col_label'>
                            <label class='form_field'>Nom</label> <label class='form_required' >*</label> </div>
                            <div class='col_field'>
                            <input type="text" name="field_0"  id="field_0" value="<?php  phpfmg_hsc("field_0", ""); ?>" class='text_box'>
                            <div id='field_0_tip' class='instruction'></div>
                            </div>
                        </li>

                        <li class='field_block' id='field_1_div'><div class='col_label'>
                            <label class='form_field'>Prénom</label> <label class='form_required' >*</label> </div>
                            <div class='col_field'>
                            <input type="text" name="field_1"  id="field_1" value="<?php  phpfmg_hsc("field_1", ""); ?>" class='text_box'>
                            <div id='field_1_tip' class='instruction'></div>
                            </div>
                        </li>

                        <li class='field_block' id='field_2_div'><div class='col_label'>
                            <label class='form_field'>Email</label> <label class='form_required' >*</label> </div>
                            <div class='col_field'>
                            <input type="text" name="field_2"  id="field_2" value="<?php  phpfmg_hsc("field_2", ""); ?>" class='text_box'>
                            <div id='field_2_tip' class='instruction'></div>
                            </div>
                        </li>

                        <li class='field_block' id='field_3_div'><div class='col_label'>
                            <label class='form_field'>Message</label> <label class='form_required' >*</label> </div>
                            <div class='col_field'>
                            <textarea name="field_3" id="field_3" cols="30" rows="10" value="<?php  phpfmg_hsc("field_3", ""); ?>" class='text_box'></textarea>
                            <div id='field_3_tip' class='instruction'></div>
                            </div>
                        </li>

                        <li class='field_block' id='field_4_div'><div class='col_label'>
                            <label class='form_field'>Objet</label> <label class='form_required' >*</label> </div>
                            <div class='col_field'>
                            <?php phpfmg_dropdown( 'field_4', "---|Postuler|Demander un devis|Obtenir un renseignement|Autre" );?>
                            <div id='field_4_tip' class='instruction'></div>
                            </div>
                        </li>

                        <li class='field_block' id='field_5_div'><div class='col_label'>
                            <label class='form_field'>Pièce-jointe</label> <label class='form_required' >&nbsp;</label> </div>
                            <div class='col_field'>
                            <input type="file" name="field_5"  id="field_5" value="" class='text_box' onchange="fmgHandler.check_upload(this);">
                            <div id='field_5_tip' class='instruction'></div>
                            </div>
                        </li>


                        <li class='field_block' id='phpfmg_captcha_div'>
                            <div class='col_label'></div><div class='col_field'>
                            <?php phpfmg_show_captcha(); ?>
                            </div>
                        </li>


                        <li>
                        <div class='col_label'>&nbsp;</div>
                        <div class='form_submit_block col_field'>
                

                        <input type='submit' value='Envoyer' class='form_button'>

                        <div id='err_required' class="form_error" style='display:none;'>
                            <label class='form_error_title'>Veuillez remplir tous les champs obligatoires</label>
                        </div>
                        


                        <span id='phpfmg_processing' style='display:none;'>
                            <img id='phpfmg_processing_gif' src='<?php echo PHPFMG_ADMIN_URL . '?mod=image&amp;func=processing' ;?>' border=0 alt='Processing...'> <label id='phpfmg_processing_dots'></label>
                        </span>
                        </div>
                        </li>

                    </ol>
                </form>

                <iframe name="submitToFrame" id="submitToFrame" src="javascript:false" style="position:absolute;top:-10000px;left:-10000px;" /></iframe>

                </div>
                <!-- end of form container -->


                <!-- [Your confirmation message goes here] -->
                <div id='thank_you_msg' style='display:none;'>
                Votre message a bien été envoyé, nous nous efforçons d'y répondre dans les plus brefs délais.!
                </div>
        </div>
    </section>
</main>







            






<?php

    phpfmg_javascript($sErr);

}
# end of form




function phpfmg_form_css(){
    $formOnly = isset($GLOBALS['formOnly']) && true === $GLOBALS['formOnly'];
?>
<style type='text/css'>

ol.phpfmg_form{
    list-style-type:none;
    padding:0px;
    margin:0px;
}

ol.phpfmg_form li{
    margin-bottom:5px;
    clear:both;
    display:block;
    overflow:hidden;
	width: 100%
}


.form_field, .form_required{
    font-weight : bold;
}

.form_required{
    color:red;
    margin-right:8px;
}

.field_block_over{
}

.form_submit_block{
    padding-top: 3px;
}

.text_box,.text_select {
    height: 32px;
}

.text_box, .text_area, .text_select {
    min-width:160px;
    max-width:300px;
    width: 100%;
    margin-bottom: 10px;
}

.text_area{
    height:80px;
}

.form_error_title{
    font-weight: bold;
    color: red;
}

.form_error{
    background-color: #F4F6E5;
    border: 1px dashed #ff0000;
    padding: 10px;
    margin-bottom: 10px;
}

.form_error_highlight input{
    border: 1px solid #ff0000;
}

div.instruction_error input{
    border: 1px solid red;
}

hr.sectionbreak{
    height:1px;
    color: #ccc;
}

#one_entry_msg{
    background-color: #F4F6E5;
    border: 1px dashed #ff0000;
    padding: 10px;
    margin-bottom: 10px;
}


#frmFormMailContainer input[type="submit"]{
    padding: 10px 25px; 
    font-weight: bold;
    margin-bottom: 10px;
    background-color: #043B8A;
}

#frmFormMailContainer input[type="submit"]:hover{
    background-color: #258FD6;
}

<?php phpfmg_text_align();?>    



</style>

<?php
}
?>

<?php include(dirname(__FILE__).'/footer.php'); ?>