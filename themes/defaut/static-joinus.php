<?php include(dirname(__FILE__).'/header.php'); ?>

	<!-- ============= MAIN CONTENT ================ -->
	<main class="main grid" role="main">
		
		<!-- ======== BLOCS FULL WIDTH ======== -->
		<section class="row">
			<div class="bloc_titre_inter col-xs-12">
				<h2>Nous rejoindre</h2>					
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC Rejoindre ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Rejoindre un groupe d'expérience</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="visuel col-xs-12 col-sm-3">
					<img src="data/medias/rejoindre-groupe.jpg" alt="">
				</div>

				<div class="content col-xs-12 col-sm-9">
					<p>Notre expérience de plus de 10 ans nous apporte un positionnement solide sur le marché. Nos nombreuses références dans différents secteurs d’activité attestent de la qualité de notre travail. Nous avons su gagner la confiance des grandes entreprises qui nous appellent régulièrement pour nous confier de nouvelles missions. <br><br>

					Nous assumons une volonté d’intégrer au sein de notre structure, les profils les plus prometteurs, toujours à la recherche de nouveaux projets ambitieux.
					</p>
				</div>
			</div>
		</section>

		<div class="row inner-page">&nbsp;</div>

		<!-- ======== BLOC Evolution ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Évolution de carrière</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="visuel col-xs-12 col-sm-3">
					<img src="data/medias/rejoindre-evolution-carriere.jpg" alt="">
				</div>

				<div class="content col-xs-12 col-sm-9">
					<p>Nous effectuons une analyse de chaque profil afin d’apporter les missions les plus appropriées à chacun de nos consultants. La source de la valeur ajoutée d’un service réside pour nous, dans l’épanouissement de nos consultants.<br><br>

					- Nos équipes commerciales et ressources humaines travaillent de pair pour un suivi spécifique de chacun de nos consultants.<br><br>

					- Nous misons sur le développement des compétences à travers des programmes de missions et de formations constituant un véritable tremplin à des fonctions de conseil, d’expertise technique et de direction de projet.
					</p>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC VALEURS ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Un groupe, des valeurs</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="visuel col-xs-12 col-sm-3">
					<img src="data/medias/rejoindre-valeurs.jpg" alt="">
				</div>

				<div class="content col-xs-12 col-sm-9">
					<p>Structure à taille humaine, nous insistons sur la proximité pour extraire le meilleur de la performance de chacun, alliant développement personnel et professionnel.<br><br>

					- Nous conjuguons professionnalisme et valeurs humaines tout en assurant le désir d’évoluer ensemble.<br>
					- Nous faisons avancer notre démarche qualité et notre engagement développement durable, aujourd’hui en plein essor.<br><br>

					Pour nous rejoindre, <a href="<?php $plxShow->urlRewrite('static4/contact-groupe-solfi'); ?>">contactez-nous</a>
					</p>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>
	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
