<?php include(dirname(__FILE__).'/header.php'); ?>

	<!-- ============= MAIN CONTENT ================ -->
	<main class="main grid" role="main">
		
		<!-- ======== BLOCS FULL WIDTH ======== -->
		<section class="row">
			<div class="bloc_titre_inter col-xs-12">
				<h2>Nos Références</h2>					
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC Assurances et retriates ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Assurance et retraites</h1>					
			</div>
			<div class="full-width-bloc inner-page row">
				<div class="col-xs-12">
					<center><img src="data/medias/references/logos_plancha1.jpg" alt=""></center>
				</div>
			</div>
		</section>

		<div class="row inner-page">&nbsp;</div>

		<!-- ======== BLOC Finance ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Banque et finance</h1>					
			</div>
			<div class="full-width-bloc inner-page row">
				<div class="col-xs-12">
					<center><img src="data/medias/references/logos_plancha2.jpg" alt=""></center>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<!-- ======== BLOC VALEURS ======== -->
		<section class="row inner-page">
			<div class="full-width-bloc inner-page col-xs-12">
				<h1>Témoignages client</h1>					
			</div>
			<div class="full-width-bloc inner-page col-xs-12">
				<div class="row" id="temoignage1">
					<div class="visuel col-xs-12 col-sm-4 col-lg-3">
						<img src="data/medias/rejoindre-valeurs.jpg" alt="">
					</div>

					<div class="content col-xs-12 col-sm-8 col-lg-9">
						<h3>« Une relation de confiance s’est installée et je sais que je peux compter sur eux… »</h3>
						<p>Nous sommes client du Groupe Solfi depuis 15 ans maintenant et je dois dire que je n’ai jamais pensé à changer de prestataire car une relation de confiance s’est installée et je sais que je peux compter sur eux. Nous avons rencontré le Groupe Solfi dans le cadre d’un appel d’offre pour configurer la structure réseau de l’entreprise. <br><br>

						En plus d’une problématique budgétaire, nous avons une structure internationale qui nous impose d’être disponible et réactif à tout moment de la journée. Le Groupe Solfi a su nous conseiller en favorisant la solution qui n’était pas la plus simple à mettre en place mais bel et bien la plus adaptée à notre situation. Le professionnalisme du Groupe Solfi nous a vite convaincu et nous leur avons depuis, confié tous nos projets qu’ils soient d’ordre matériel ou stratégique.
						</p>
					</div>
				</div>

				<div class="row" id="temoignage2">
					<div class="visuel col-xs-12 col-sm-4 col-lg-3">
						<img src="data/medias/rejoindre-valeurs.jpg" alt="">
					</div>

					<div class="content col-xs-12 col-sm-8 col-lg-9">
						<h3>« Très vite nous avons senti que nous étions face à une équipe compétente »	</h3>
						<p>Nous avons rencontré le groupe Solfi pour le renouvellement de notre parc informatique. Très vite nous avons senti que nous étions face à une équipe compétente qui nous a donné envie de leur confié un projet beaucoup plus gros. Nous devions en parallèle intégrer la virtualisation de nos systèmes et renforcer la sécurité de nos données. Non seulement le Groupe Solfi nous a apporté les réponses attendues mais en leur confiant cette double mission, il nous a permis de faire des économies !
						</p>
					</div>
				</div>

				<div class="row" id="temoignage3">
					<div class="visuel col-xs-12 col-sm-4 col-lg-3">
						<img src="data/medias/rejoindre-valeurs.jpg" alt="">
					</div>

					<div class="content col-xs-12 col-sm-8 col-lg-9">
						<h3>« Pour la mise en place de la virtualisation de nos systèmes… il nous fallait un tiers de confiance qui nous connaît bien »</h3>
						<p>Nous sommes client du groupe Solfi depuis plusieurs années et quand nous avons eu besoin de dématérialiser nos serveurs, il était évident de faire appel à leurs services. Mettre en place la virtualisation par le cloud est source de stress pour toute entreprise. Les questions de sauvegarde et sécurité des données sont primordiales.<br><br>

						En plus de faire appel à une équipe compétente, nous avions besoin qu’elle soit capable de nous rassurer. Il nous fallait un tiers de confiance qui nous connaît bien. Le transfert s’est passé sans encombre et nous sommes ravis de la solution mise en place surtout que nous pouvons toujours compter sur eux pour la faire évoluer ou nous dépanner si besoin.
						</p>
					</div>
				</div>
			</div>
		</section>

		<div class="row">&nbsp;</div>


	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
