<?php include(dirname(__FILE__).'/header.php'); ?>
<?php
if( isset($_POST['send_mail'])){
	$etat = "erreur";

	// Mise en forme des données
	if (isset($_POST["nom"])) $_POST["nom"]=trim(stripslashes($_POST["nom"]));
	if (isset($_POST["prenom"])) $_POST["prenom"]=trim(stripslashes($_POST["prenom"]));
	if (isset($_POST["sender_email"])) $_POST["sender_email"]=trim(stripslashes($_POST["sender_email"]));
	if (isset($_POST["objet"])) $_POST["objet"]=trim(stripslashes($_POST["objet"]));
	if (isset($_POST["message"])) $_POST["message"]=trim(stripslashes($_POST["message"]));

	// Vérification des erreurs
	if (empty($_POST["nom"])) { // L'utilisateur n'a pas rempli le champ email 
		$erreur="Vous n'avez pas renseigné votre nom"; // On met dans erreur le message qui sera affiché
	}

	if (empty($_POST["prenom"])) { 
		$erreur="Vous n'avez pas renseigné votre prénom";
	}
	
	elseif (!preg_match("$[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,4}$",$_POST["sender_email"])){ 
		$erreur="Votre adresse e-mail n'est pas valide...";
	}
	
	elseif (empty($_POST["message"])) { 
		$erreur="Vous n'avez pas entré de message";
	}
	
	else { // Si tous les champs sont valides, on change l'état à ok
		$etat="ok";
	}


	if ($etat!="ok"){ // Le formulaire a été soumis mais il y a des erreurs (etat=erreur) OU le formulaire n'a pas été soumis (etat=attente)
		if ($etat=="erreur"){ // Cas où le formulaire a été soumis mais il y a des erreurs
			echo "<p><span style=\"color:red\">".$erreur."</span></p>\n"; // On affiche le message correspondant à l'erreur
		}			
	}

	else {
		$nom = $_POST["nom"];
		$prenom = $_POST["prenom"];
		$email = $_POST["sender_email"];
		$objet = $_POST["objet"];
		$msg = $_POST["message"];

		$contenu_mail = 'Un formulaire de contact a été envoyé:<br><br>
		'.$nom.' '.$prenom.'<br>
		'.$email.'<br>
		objet : '.$objet.'<br><br>
		Message : '.$msg.' ';

		// Envoie du mail pour le prospect
		$to = 'perreau.gh@gmail.com';



		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		// Additional headers
		$headers .= 'From:'.$email.''."\r\n";
		'X-Mailer: PHP/'.phpversion();

			// Récupération du fichier en pièce-jointe
		if ( strlen($_FILES['attached']['name']) > 0 ) {	// If a file was uploaded, do some processing
			$filename = preg_replace('/[^a-zA-Z0-9._-]/', '', $_FILES['attached']['name']);
			$filetype = $_FILES["attached"]["type"];
			$filesize = $_FILES["attached"]["size"];
			$filetemp = $_FILES["attached"]["tmp_name"]; 
			$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1);
		
			if ( !preg_match('/\.(doc|docx|odt|pdf|rtf|txt)/i', $ext) )
			{	$erreur = "Upload filetype not supported.";
			}
			else if ( $filesize > 2000000 )
			{	$erreur = "File size too high, up to 2MB is allowed.";
			}
			else
			{	// Looks like the file is good, send it with the email
				$fp = fopen($filetemp, "rb");
				$file = fread($fp, $filesize);
				$file = chunk_split(base64_encode($file));
				
				// Attachment headers
				$headers .= "\n--".$num."\n";
				$headers .= "Content-Type:".$filetype." ";
				$headers .= "name=\"".$filename."\"r\n";
				$headers .= "Content-Disposition: attachment; ";
				$headers .= "filename=\"".$filename."\"\n";
				$headers .= "".$file."";
				
			}
		}
		
		//Envoi du mail 
		if (!mail($to, $objet, $contenu_mail, $headers )) {
			echo 'Le formulaire n\'a pas été envoyé';
		}

		else {
		
			echo "Formulaire envoyé !";
			header("Location: http://127.0.0.1/Solfi_pluxml/");
      		exit;
		}
	}
};
?>
	<main class="main grid" role="main">
		<section class="row">
			<div class="bloc_titre_inter col-xs-12">
				<h2>Une candidature ? Un renseignement ?</h2>					
			</div>
		</section>

		<div class="row">&nbsp;</div>

		<section class="row inner-page">
			<p>Le Groupe solfi se tient à votre disposition. Pour tout renseignement ou candidature spontanée, veuillez remplir le formulaire ci-dessous. </p>
			<!-- Début du formulaire -->
			<form action="" method="post" enctype="multipart/form-data">
			<!-- <form action="themes/defaut/php/envoi_contact.php" method="post"> -->
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label for="nom">Nom :</label>
						<input type="text" value="" name="nom">
					</div>
				</div>		

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label for="prenom">Prénom :</label>
						<input type="text" value="" name="prenom">
					</div>
				</div>	

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label for="sender_email">E-mail (obligatoire) :</label>
						<input type="mail" value="" name="sender_email">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label for="objet">Objet :</label>
						<select name="objet" id="objet">
							<option value="">---</option>
							<option value="postuler">Postuler</option>
							<option value="demander_devis">Demander un devis</option>
							<option value="renseignement">Obtenir un renseignement</option>
							<option value="autre">Autre</option>
						</select>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label for="msg">Votre message</label>
						<textarea name="message" id="msg" cols="30" rows="10"></textarea>
					</div>		
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<label for="attached">Pièce jointe</label>
						<input type="file" name="attached" id="attached">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<input type="submit" name="send_mail" id="send_mail" value="Envoyer">
					</div>
				</div>
			</form>
		</section>
	</main>
	

<?php include(dirname(__FILE__).'/footer.php'); ?>
