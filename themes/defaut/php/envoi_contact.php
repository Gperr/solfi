<?php
if( isset($_POST['send_mail'])){
	$etat = "";

	// Mise en forme des données
	if (isset($_POST["nom"])) $_POST["nom"]=trim(stripslashes($_POST["nom"]));
	if (isset($_POST["prenom"])) $_POST["prenom"]=trim(stripslashes($_POST["prenom"]));
	if (isset($_POST["sender_email"])) $_POST["sender_email"]=trim(stripslashes($_POST["sender_email"]));
	if (isset($_POST["objet"])) $_POST["objet"]=trim(stripslashes($_POST["objet"]));
	if (isset($_POST["message"])) $_POST["message"]=trim(stripslashes($_POST["message"]));

	// Vérification des erreurs
	if (empty($_POST["nom"])) { // L'utilisateur n'a pas rempli le champ email 
		$erreur="Vous n'avez pas renseigné votre nom"; // On met dans erreur le message qui sera affiché
	}

	if (empty($_POST["prenom"])) { 
		$erreur="Vous n'avez pas renseigné votre prénom";
	}
	
	elseif (!preg_match("$[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,4}$",$_POST["sender_email"])){ 
		$erreur="Votre adresse e-mail n'est pas valide...";
	}
	
	elseif (empty($_POST["message"])) { 
		$erreur="Vous n'avez pas entré de message";
	}
	
	else { // Si tous les champs sont valides, on change l'état à ok
		$etat="ok";
	}


	if ($etat!="ok"){ // Le formulaire a été soumis mais il y a des erreurs (etat=erreur) OU le formulaire n'a pas été soumis (etat=attente)
		if ($etat=="erreur"){ // Cas où le formulaire a été soumis mais il y a des erreurs
			echo "<p><span style=\"color:red\">".$erreur."</span></p>\n"; // On affiche le message correspondant à l'erreur
		}			
	}

	else {
		$nom = $_POST["nom"];
		$prenom = $_POST["prenom"];
		$email = $_POST["sender_email"];
		$objet = $_POST["objet"];
		$msg = $_POST["message"];

		$contenu_mail = 'Un formulaire de contact a été envoyé:<br><br>
		'.$nom.' '.$prenom.'<br>
		'.$email.'<br>
		objet : '.$objet.'<br><br>
		Message : '.$msg.' ';

		// Envoie du mail pour le prospect
		$to = 'perreau.gh@gmail.com';

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		// Additional headers
		$headers .= 'From:'.$email.''."\r\n";
		'X-Mailer: PHP/'.phpversion();
		// $headers .= 'Bcc: Ghislain Perreau <gperreau@sollyazar.com>, Service Commercial <servicecommercial@sollyazar.com>' . "\r\n"; 
		  
		
		//Envoi du mail 
		if (!mail($to, $objet, $contenu_mail, $headers )) {
			echo 'Le formulaire n\'a pas été envoyé';
		}

		else {
			echo "Formulaire envoyé !";
		}
	}
};