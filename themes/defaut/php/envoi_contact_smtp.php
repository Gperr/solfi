<?php
if( isset($_POST['send_mail'])){
	$etat = "";

	// Mise en forme des données
	if (isset($_POST["nom"])) $_POST["nom"]=trim(stripslashes($_POST["nom"]));
	if (isset($_POST["prenom"])) $_POST["prenom"]=trim(stripslashes($_POST["prenom"]));
	if (isset($_POST["sender_email"])) $_POST["sender_email"]=trim(stripslashes($_POST["sender_email"]));
	if (isset($_POST["objet"])) $_POST["objet"]=trim(stripslashes($_POST["objet"]));
	if (isset($_POST["message"])) $_POST["message"]=trim(stripslashes($_POST["message"]));

	// Vérification des erreurs
	if (empty($_POST["nom"])) { // L'utilisateur n'a pas rempli le champ email 
		$erreur="Vous n'avez pas renseigné votre nom"; // On met dans erreur le message qui sera affiché
	}

	if (empty($_POST["prenom"])) { 
		$erreur="Vous n'avez pas renseigné votre prénom";
	}
	
	elseif (!preg_match("$[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,4}$",$_POST["sender_email"])){ 
		$erreur="Votre adresse e-mail n'est pas valide...";
	}
	
	elseif (empty($_POST["message"])) { 
		$erreur="Vous n'avez pas entré de message";
	}
	
	else { // Si tous les champs sont valides, on change l'état à ok
		$etat="ok";
	}


	if ($etat!="ok"){ // Le formulaire a été soumis mais il y a des erreurs (etat=erreur) OU le formulaire n'a pas été soumis (etat=attente)
		if ($etat=="erreur"){ // Cas où le formulaire a été soumis mais il y a des erreurs
			echo "<p><span style=\"color:red\">".$erreur."</span></p>\n"; // On affiche le message correspondant à l'erreur
		}			
	}

	else {
		$nom = $_POST["nom"];
		$prenom = $_POST["prenom"];
		$email = $_POST["sender_email"];
		$objet = $_POST["objet"];
		$msg = $_POST["message"];

		$contenu_mail = 'Un formulaire de contact a été envoyé:<br><br>
		'.$nom.' '.$prenom.'<br>
		'.$email.'<br>
		objet : '.$objet.'<br><br>
		Message : '.$msg.' ';


		//Envoi du mail 
	    require 'PHPMailer/class.phpmailer.php';
		require 'PHPMailer/class.smtp.php';

		$mail_contact = new PHPMailer;

		$mail_contact->SMTPDebug = 1;                               // Enable verbose debug output

		$mail_contact->isSMTP();                                      // Set mailer to use SMTP
		$mail_contact->Host = 'smtp.groupe-solfi.fr.';  						  // Specify main and backup SMTP servers
		$mail_contact->SMTPAuth = false;                               // Enable SMTP authentication
		$mail_contact->Username = 'infos@groupe-solfi.fr';                 // SMTP username
		$mail_contact->Password = 'Solfismtp11';                           // SMTP password
		$mail_contact->SMTPSecure = '';                             // Enable TLS encryption, `ssl` also accepted
		$mail_contact->Port = 25;                                    // TCP port to connect to

		$mail_contact->CharSet = 'UTF-8';
		$mail_contact->From = $email;
		$mail_contact->FromName = $nom;
		$mail_contact->addAddress('solfi@groupe-solfi.fr');     // Add a recipient
		//$mail_contact->addAddress('ellen@example.com');               // Name is optional
		$mail_contact->addReplyTo($email);
		//$mail_contact->addCC('gperreau@sollyazar.com');

		//$mail_contact->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail_contact->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		$mail_contact->isHTML(true);                                  // Set email format to HTML

		$mail_contact->Subject = 'Formulaire de contat '.$objet.'';
		$mail_contact->Body    = $msg;
		$mail_contact->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail_contact->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail_contact->ErrorInfo;
		} else {
			// header('contact-groupe-solfi');
		    echo 'Message has been sent';
		}	

	}
};