<?php if (!defined('PLX_ROOT')) exit; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
<head>
	<meta charset="<?php $plxShow->charset('min'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->pageTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>
	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/plucss.css" media="screen"/>
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/theme.css" media="screen"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>

	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/bootstrap/css/bootstrap.min.css" media="screen"/>
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/styles.css" media="screen"/>


	<?php $plxShow->templateCss() ?>
	<?php $plxShow->pluginsCss() ?>
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
</head>

<body id="top">

	<!-- =============== Header =============== -->
	<header class="header col-xs-12" role="banner">
		<div class="container head_wrapper">
			<div class="col-xs-7 col-lg-3 col-xl-4">
				<a href="<?php $plxShow->racine() ?>"><img class="logo" src="data/medias/logo-groupe-solfi.svg" alt="Groupe Solfi"></a>
			</div>

			<div class="col-xs-5 menu_resp_trigger">
				<div class="btn_menu">
					<img src="data/medias/btn_menu.jpg" alt="+">
				</div>
			</div>

			<div class="menu_wrapper col-xs-12 col-lg-9 col-xl-8">
				<nav class="nav" role="navigation">
					<div class="responsive-menu col-xs-12">
						<ul class="menu expanded">
							<?php $plxShow->staticList($plxShow->getLang('HOME'),'<li class="#static_status" id="#static_id"><a href="#static_url" title="#static_name">#static_name</a></li>'); ?>
							<?php $plxShow->pageBlog('<li id="#page_id"><a class="#page_status" href="#page_url" title="#page_name">#page_name</a></li>'); ?>
						</ul>
					</div>

					<div class="responsive-menu-mobile col-xs-12">
						<ul class="menu expanded">
							<?php $plxShow->staticList($plxShow->getLang('HOME'),'<li class="#static_status" id="#static_id"><a href="#static_url" title="#static_name">#static_name</a></li>'); ?>
							<?php $plxShow->pageBlog('<li id="#page_id"><a class="#page_status" href="#page_url" title="#page_name">#page_name</a></li>'); ?>
						</ul>
					</div>
				</nav>
			</div>
		</div>

		<img class="bandeau_bleu" src="data/medias/bandeau_blue.png" alt="">
	</header>

<div class="container">

	
